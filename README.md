# IO Energy's Website
---

Welcome to the git repo for the IO Energy Website made by Hackamatix.


## Home Page
---
The home page is currently live and can be found @ [https://ioenergy.com.au/](https://ioenergy.com.au/)

![Home Page Img](Screenshots/Home.png)

## Our Plans Page
---
This is a page where customers can view plans, estimate costs and calculate their potential savings. It uses a proprietary algorithm to calculate how much customers can potentially save on our plans.

This page can be found @ [https://hackamatix1.imfast.io/OurPlans/ourPlans.html](https://hackamatix1.imfast.io/OurPlans/ourPlans.html) but is not for public use yet.

![Plans Page Img](Screenshots/Plans.png)

## Survey Page
---
This is a survey that customers will be directed too prior to a phone call with a customer rep. This is to gain more information about their consumption habits so that we can better understand how to save them money.

This page can be found @ [https://hackamatix1.imfast.io/Survey/survey.html](https://hackamatix1.imfast.io/Survey/survey.html) but is not for public use yet.

![Survey Page Img](Screenshots/Survey.png)

<?php

if (isset($_POST['submit'])) {
    // Information From Form -- Name Email PhoneNo  SmartMeter Notes
    $howHear = str_replace(",", "|", $_POST['HowHear']);
    $ownership = str_replace(",", "|", $_POST['Ownership']);
    $whyUs = str_replace(",", "|", $_POST['WhyUs']);
    $email = str_replace(",", "|",$_POST['email']);

    // Write txt file with customer data
    $myfile = fopen('../uploads/ThankYouResponses.csv', "a");
    if ( !$myfile ) {
        header('Location: ../ThankYou/?Error');
        exit();
    }
    $txt = $howHear.",".$ownership.",".$whyUs.",".$email."\n";
    fwrite($myfile, $txt);
    fclose($myfile);
    header("Location: ../ThankYou/?SuccessfulUpload"); 
    exit();
}

?>
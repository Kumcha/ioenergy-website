$(document).ready(function() {
    // Handle URL Msgs
    // - This handles the status of upload msgs passed in via the URL.
    var currentURL = window.location.href; // The actual url e.g https://www.ioenergy.com.au/OurPlans/?SuccessfulUpload

    var URLmsg = currentURL.split('?').pop(); // = The msg we are interested in
    console.log(URLmsg)

    showUploadResultMessage(URLmsg);

    // On closing of Upload Modal the url should be cleared
    $('#SubmitMsg').on('hide.bs.modal', function () {
        console.log("Page Closed");
        window.history.pushState({}, null, "/ThankYou/");
    });


});


// Function to show the correct message on loading the page, Specifically for the file upload.
function showUploadResultMessage(message) {
    var errorMsgsMap = new Map();

    errorMsgsMap.set("SuccessfulUpload", new uploadResultMessage("SuccessfulUpload", "Your upload was successful!", "assets/successImg.svg"));
    errorMsgsMap.set("TooBig_2MB", new uploadResultMessage("TooBig_2MB", "Your file was too big! Keep the size under 2MB.", "assets/errorImg.svg"));
    errorMsgsMap.set("Error", new uploadResultMessage("Error", "Something went wrong!<br>Please try again or contact us via email.<br><a class=' underlineLink' href='mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site'>save@ioenergy.com.au</a>", "assets/errorImg.svg"));
    errorMsgsMap.set("TypeError", new uploadResultMessage("TypeError", "Wrong Type. Please only upload .jpg .png or .pdf.", "assets/errorImg.svg"));
    errorMsgsMap.set("Unfinished", new uploadResultMessage("Unfinished", "You didn't give us enough information!", "assets/errorImg.svg"));
    errorMsgsMap.set("Uncaught", new uploadResultMessage("Uncaught", "Ooops, Something went wrong! Please try again!", "assets/errorImg.svg"));

    if (errorMsgsMap.has(message)){

        $('#UploadStatusMsgImg').attr("src", errorMsgsMap.get(message).img);
        $('#UploadStatusMsgText').append(errorMsgsMap.get(message).msg);

        $('#SubmitMsg').modal('show');

    } else {
    }
}
// Class to hold details about each msg
class uploadResultMessage {
    constructor(msgCode, msg, img) {
      this.msgCode = msgCode;
      this.msg = msg;
      this.img = img;
    }
}

In order to ensure that no unauthorised access to the 'ThankYouResponses.csv' 
file, please ensure that if the file is deleted and replaced the permissions 
is set to 700. To do this right click on the file and click 'Change Permissions'

When the data has been viewed and is need of clearing, delete all lines 
excluding the very first line. This line acts as a header.

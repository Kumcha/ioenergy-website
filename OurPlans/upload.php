<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
/* Exception class. */
require 'PHPMailer\src\Exception.php';

/* The main PHPMailer class. */
require 'PHPMailer\src\PHPMailer.php';

/* SMTP class, needed if you want to use SMTP. */
require 'PHPMailer\src\SMTP.php';

// echo "Hello World";

if (isset($_POST['submit'])) {
    // **File Validation**
    // The selected file's details - [name, type, tmp_name, error, size]
    $file = $_FILES['bill'];

    // All properties of the file
    $fileName = $file['name'];
    $fileTmpName = $file['tmp_name'];
    $fileSize = $file['size'];
    $fileError = $file['error'];
    $fileType = $file['type'];

    // Information From Form -- Name Email PhoneNo  SmartMeter Notes
    $customerName = $_POST['name'];
    $customerEmail = $_POST['email'];
    $customerPhone = $_POST['phone'];
    $smartMeterConsent = $_POST['MeterAnalysisRadioButtons'];
    $notes = $_POST['notes'];


    // Split on '.' -> last item of split array -> lowercase
    $fileExt = strtolower(end(explode('.', $fileName)));

    // Acceptable types
    $allowedExtensions = array('jpg', 'jpeg', 'png', 'pdf');

    // Validation
    if (in_array($fileExt, $allowedExtensions)) { // Is it appropriate type
         if ($fileError === 0){ // No errors
            if ($fileSize < 2000000) { // No more than 2mb

                // ********** Code to Upload File and save on file system **********

                // $uniqueID =  uniqid('', true);
                // $fileNameNew =  $uniqueID.".".$fileExt;

                // // Made our destination Directory
                // mkdir('../uploads/'.$uniqueID, 0722);

                // $fileDestination = '../uploads/'.$uniqueID.'/'.$fileNameNew; //Destination
                // move_uploaded_file($fileTmpName, $fileDestination);

                // // Write txt file with customer data
                // $myfile = fopen('../uploads/'.$uniqueID.'/'.$uniqueID.'.txt', "w") or die("Unable to open file!");
                // $txt = $customerName."\n".$customerEmail."\n".$customerPhone."\n";
                // fwrite($myfile, $txt);
                // fclose($myfile);

                // ****************************************************************

                try {

                    // Create New Email Using PHPMailer
                    $email = new PHPMailer(TRUE);

                    //Server settings
                    // $email->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
                    $email->isSMTP();                                            // Send using SMTP
                    $email->Host       = 'ioenergy.com.au';                    // Set the SMTP server to send through
                    $email->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $email->Username   = 'site@ioenergy.com.au';                     // SMTP username
                    $email->Password   = 'ioEnergy!';                               // SMTP password
                    $email->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                    $email->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                    // From and To
                    $email->setFrom('site@ioenergy.com.au', 'New Bill Enquiry');
                    $email->AddAddress( 'save@ioenergy.com.au' );

                    // Content
                    $email->isHTML(true); // Set email format to HTML
                    $email->Subject = 'New Bill Enquiry -- Website';
                    $email->Body    = '<b>Name: </b>'.$customerName.'<br><b>Email: </b>'.$customerEmail.'<br><b>PhoneNo: </b>'.$customerPhone.'<br><b>Smart Meter Consent: </b>'.$smartMeterConsent.'<br><b>Notes: </b>'.$notes;
                    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                    $email->addAttachment($fileTmpName, 'uploadedFile.'.$fileExt);


                    $email->send();
                    // /* Redirect browser */
                    header("Location: ../OurPlans/?SuccessfulUpload"); 
                    exit();


                } catch (Exception $e) {
                    // echo "Message could not be sent. Mailer Error: {$email->ErrorInfo}";
                    // Error Uploading
                    header('Location: ../OurPlans/?Error');
                    exit();
                }
            } else {
                // File too big
                header('Location: ../OurPlans/?TooBig_2MB');
                exit();
            }
         } else {
             // Error Uploading
             header('Location: ../OurPlans/?Error');
             exit();
         }
    } else {
        // Alert Bad File Type
        header('Location: ../OurPlans/?TypeError');
        exit();
    }

} else {
    header('Location: ../OurPlans/?Unfinished');
    exit();
}

header('Location: ../OurPlans/?Uncaught');
die();
?>
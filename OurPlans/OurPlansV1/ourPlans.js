// Global Variables
// Stores all postcodes in AUS used in postcode autofil
var postcodes = {};

// Used to redirect to appropriate EL Sign Up Page
var elURL = "";

// This obj stores all the relevant gtags for each plan
var plansGtags = {'spark' : 'AW-656750783/BSzsCNbVn94BEL_xlLkC',
                 'millennium' : 'AW-656750783/MGuSCI7Vu94BEL_xlLkC', 
                 'lightning': 'AW-656750783/VPNRCMrVu94BEL_xlLkC',
                 'jupiter' : 'AW-656750783/v20pCJmJqt4BEL_xlLkC',
                 'pulse' : 'AW-656750783/hxwxCL7bn94BEL_xlLkC',
                 'pulseplus' : 'AW-656750783/INhtCMn9qd4BEL_xlLkC'
                };

$(document).ready(function() {
    initialiseInputs();

    // 
   
        // $("#UploadBillModal").on('shown.bs.modal', function(){
        //     $('#recipient-name').focus();
        // });
    
//    $("#DisclaimerModal").modal('show');

    // Handle URL Msgs
    // - This handles the status of upload msgs passed in via the URL.
    var currentURL = window.location.href; // The actual url e.g https://www.ioenergy.com.au/OurPlans/?SuccessfulUpload

    var URLmsg = currentURL.split('?').pop(); // = The msg we are interested in
    console.log(URLmsg)

    showUploadResultMessage(URLmsg);

    // On closing of Upload Modal the url should be cleared
    $('#UploadBillModal').on('hide.bs.modal', function () {
        console.log("Page Closed");
        $('#BillUploadForm').show();
        $('#UploadStatusMsgContainer').hide();
        window.history.pushState({}, null, "/OurPlans/");
    });


    // Things to hide on boot
    $("#BusinessPlans").hide();
    $("#ControlledInput").hide();
    $("#ControlledInput2").hide();
    $("#SolarInput").hide();
    $("#SignupLight").hide();
    $("#SignupJupiter").hide();


    // Set Values to default
    $("#AnnualUseInputBox").val(4000);
    $("#AnnualUseInputBox2").val(4000);


    // Linking Annual Use Input boxes
    //- This section ensures that the duplicated boxes are identical when inputs are modified by the user but not when buttons are clicked
    // Annual use 1 to 2
    $("#AnnualUseInputBox").keyup(function() {
        $("#AnnualUseInputBox2").val( this.value );
    });
    $("#AnnualUseInputBox").blur(function() {
        $("#AnnualUseInputBox2").val( this.value );
    });
    // Annual use 2 to 1
    $("#AnnualUseInputBox2").keyup(function() {
        $("#AnnualUseInputBox").val( this.value );
    });
    $("#AnnualUseInputBox2").blur(function() {
        $("#AnnualUseInputBox").val( this.value );
    });
    // End Linking Annual Use Input boxes

    // Linking controlled use input boxes
    // Controlled use 1 to 2
    $("#ControlledLoadInputBox").keyup(function() {
        $("#ControlledLoadInputBox2").val( this.value );
    });
    $("#ControlledLoadInputBox").blur(function() {
        $("#ControlledLoadInputBox2").val( this.value );
    });
    // Controlled use 2 to 1
    $("#ControlledLoadInputBox2").keyup(function() {
        $("#ControlledLoadInputBox").val( this.value );
    });
    $("#ControlledLoadInputBox2").blur(function() {
        $("#ControlledLoadInputBox").val( this.value );
    });
    // End linking controlled use input boxes

 
    // Making the number of people disappear based on if they are a business or residential
    var businessButton = 0;
    var businessDefaults = 6;
    $("input[type=radio][name=TypeOfPlansRadioButtons]").change(function() {
        // $(".percOffMoreThanText").show();
        $(".percOffText").show();
        $('#Default').prop('checked', true);
        $("#BillKnown").fadeOut(500);
        $("#BillUnknown").fadeOut(500);
        $("#JupiterAnnualBill").css("visibility", "hidden");
        $("#JupiterComparison").css("visibility", "hidden");
        $("#ControlledInput2").fadeOut();
        $('#2People').prop('checked', true);

        // If BUSINESS
        if (this.value == 1) {
            $("#ResidentialPlans").fadeOut(500);
            $("#BusinessPlans").delay(500).fadeIn();

            $("#NoOfPeopleRadioButtonsContainer").fadeOut(500);
            $("#off-peak-sliderLabel").fadeOut(500)
            $("#off-peak-slider").fadeOut(500)

            businessButton = 1;
            displayInputs(businessDefaults, controlledUseButton, businessButton);
            displaySavingsReimagined(20000, 0, 0, 0.2246, 1, 0)
        } 
        // If RESIDENTIAL
        else {
            $("#BusinessPlans").fadeOut(500);
            $("#ResidentialPlans").delay(500).fadeIn();

            $("#NoOfPeopleRadioButtonsContainer").fadeIn(500);
            $("#off-peak-sliderLabel").fadeIn(500)
            $("#off-peak-slider").fadeIn(500)

            businessButton = 0;
            controlledUseButton = 0;
            selectedPeople = 2;
            displayInputs(selectedPeople, controlledUseButton, businessButton);
            displaySavingsReimagined(4000, 0, 0, 0.2246, 1, 0)
        }
    });

     /* Change what value displays in annual input and controlled load when amount of people, controlled load, and business are changed*/
     var noOfPeopleDefaults = {
        1 : [3100, 3255, 1395],
        2 : [4000, 4200, 1800],
        3 : [4745, 4982.25, 2135.25],
        4 : [6168.5, 6476.925, 2775.825],
        5 : [6168.5, 6476.925, 2775.825],
        6 : [20000, 0, 100]
    };


    var controlledUseButton = 0;
    // On change of the controlled load radio buttons
    $("input[type=radio][name=ControlledLoadRadioButtons]").change(function() {
        controlledUseButton = this.value;

        // If there is a controlled load 
        if (controlledUseButton == 1) {

            //show the input box
            $("#ControlledInput").fadeIn(500);
            $("#ControlledInput2").fadeIn(500);

            // If business display the appropriate defaults
            if (businessButton == 1) {
                displayInputs (businessDefaults, controlledUseButton, businessButton);

                //Changing the text inside the pulse plan
                $(".percOffText").hide();
            } else {
                displayInputs (selectedPeople, controlledUseButton, businessButton);
            }

            //Show or Hide controlled input 2
            $("#ControlledInput2").fadeIn(500);

        } else {
            // Remove input box
            $("#ControlledInput").fadeOut(500);
            $("#ControlledInput2").fadeOut(500);

            // If business display the appropriate defaults
            if (businessButton == 1) {
                displayInputs (businessDefaults, controlledUseButton, businessButton);

                //Changing the text inside the pulse plan
                // $(".percOffMoreThanText").hide();
                $(".percOffText").hide();
            } else {
                displayInputs (selectedPeople, controlledUseButton, businessButton);
            }

            //Show or Hide controlled input 2
            $("#ControlledInput2").fadeOut(500);
        }
       
    });

    // Making the solar feed input box appear or disappear if they have solar
    solarButton = 0;
    $("input[type=radio][name=SolarFeedRadioButtons]").change(function() {
        solarButton = this.value;

        // If they HAVE solar
        if (solarButton == 1) {
            $("#SolarInput").delay(500).fadeIn();
        }
        // If they DONT HAVE solar 
        else {
            $("#SolarInput").fadeOut(500);
        }
    })

    // Making the InterestBattery question disappear if they have a battery
    homeBattery = 0;
    $("input[type=radio][name=HomeBatteryRadioButtons]").change(function() {
        homeBattery = this.value;

        // If they HAVE home battery
        if (homeBattery == 1) {
            $("#BatteryInterestLabel").fadeOut();
            $("#BatteryInterestRadioButtons").fadeOut();

            $("#SignupLight").show();
            $("#SignupJupiter").show();
            $("#BookACallLight").hide();
            $("#BookACallJupiter").hide();
        }
        // If they DONT HAVE home battery 
        else {
            $("#BatteryInterestLabel").delay(500).fadeIn();
            $("#BatteryInterestRadioButtons").delay(500).fadeIn();

            $("#SignupLight").hide();
            $("#SignupJupiter").hide();
            $("#BookACallLight").show();
            $("#BookACallJupiter").show();
        }
    })

    // Making Modal appear if they're interested in a home battery
    batteryInterest = 0;
    $("input[type=radio][name=BatteryInterestRadioButtons]").change(function() {
        batteryInterest = this.value;

        // If they ARE interested in home battery
        if (batteryInterest == 1) {
            $("#BookACallModal").modal('show');
        }
        // If they are NOT interested 
        else {
        }
    })

    //Handler for noOfPeopleRadioButton
    var selectedPeople = 2;
    $("input[type=radio][name=NoOfPeopleRadioButtons]").change(function() {
        selectedPeople = this.value;
        // Display the appropriate defaults
        displayInputs (selectedPeople, controlledUseButton, businessButton);
    });

    // Updates all the inputs
    function displayInputs(selectedPeople, controlledUseButton, businessButton) {
        var inputGeneral = $("#AnnualUseInputBox");
        var inputGeneral2 = $("#AnnualUseInputBox2");
        var inputControlled = $("#ControlledLoadInputBox");
        var inputControlled2 = $("#ControlledLoadInputBox2");

        var selectedValues = noOfPeopleDefaults[selectedPeople];

        /* If they don't have a controlled use */
        if (controlledUseButton == 0) {
            inputGeneral.val(round5(selectedValues[0]));
            inputGeneral2.val(round5(selectedValues[0]));
            inputControlled.val(0);
            inputControlled2.val(0);
        } 
        /* If they do have a controlled use */
        else {
            if (businessButton == 1) {
                inputGeneral.val(round5(selectedValues[0]));
                inputGeneral2.val(round5(selectedValues[0]));
                inputControlled.val(round5(selectedValues[1]));
                inputControlled2.val(round5(selectedValues[1]));
            } else {
                inputGeneral.val(round5(selectedValues[1]));
                inputGeneral2.val(round5(selectedValues[1]));
                inputControlled.val(round5(selectedValues[2]));
                inputControlled2.val(round5(selectedValues[2]));
            }
        }        
    }

    // Solar Radio Button handler
    $("#SolarInputBoxContainer").hide();
    $("input[type=radio][name=SolarRadioButtons]").change(function() {
        // console.log("PEP");
        if (this.value == 0) {
            $("#SolarInputBoxContainer").fadeOut(500);
        } else {
            $("#SolarInputBoxContainer").fadeIn(500);
        }
    
    });

    // Making the compare to plan section disappear when the customer knows their bill
    $("#BillKnown").hide();
    $("#BillUnknown").hide();
    $("#JupiterComparison").css("visibility", "hidden");
    $("#PulsePlusComparison").css("visibility", "hidden");
    $("#PulsePlusAnnualBill").css("visibility", "hidden");
    $("#JupiterAnnualBill").css("visibility", "hidden");
    $("input[type=radio][name=CompareToPlanRadioButtons]").change(function() {
        // If they know their bill
        if (this.value == 1) {
            $("#DefaultNote").fadeOut(500);
            $("#BillUnknown").fadeOut(500);
            $("#BillKnown").delay(500).fadeIn(500);
            $("#JupiterComparison").css("visibility", "hidden");
        }
        // If they are comparing to competing plans
        else if (this.value == 2) {
            $("#DefaultNote").fadeOut(500);
            $("#BillKnown").fadeOut(500);
            $("#BillUnknown").delay(500).fadeIn(500);
            // $("#JupiterComparison").css("visibility", "visible");

            if (businessButton == 1) {
                $(".percOffText").hide();
                $(".percOffText").hide();
                // $("#StandardPlanSelector").val('custom');
            } 
        } 
        // If they are using default values
        else {
            $("#BillKnown").fadeOut(500);
            $("#BillUnknown").fadeOut(500);
            $("#DefaultNote").delay(500).fadeIn(500);

            // Changing the values and buttons back to default 
            selectedPeople = 2;
            $('#2People').prop('checked', true);
            $('#SolarFeedInInputBox').val(null);
            $( "#off-peak-slider" ).slider({
                value: 22
            });
            $("#Monthly").prop('checked',true);

            // BUSINESS
            if (businessButton == 1) {
                $("#AnnualUseInputBox").val(20000);

                $(".percOffText").show();

                $('#NoControlledLoad').prop('checked', true);
                $("#ControlledInput").fadeOut(500);
                $("#ControlledInput2").fadeOut(500);

                displaySavingsReimagined(20000, 0, 0, 0.2246, 1, 0)
            } 
            // RESIDENTIAL
            else {
                if (controlledUseButton == 1) {
                    displayInputs(selectedPeople, controlledUseButton, businessButton);

                    $("#AnnualUseInputBox").val(4200);
                    displaySavingsReimagined(4200, 1800, 0, 0.2246, 1, 0)
                } else {
                    controlledUseButton = 0
                    displayInputs(selectedPeople, controlledUseButton, businessButton);

                    $("#AnnualUseInputBox").val(4000);
                    displaySavingsReimagined(4000, 0, 0, 0.2246, 1, 0)
                }
            }

            $("#JupiterComparison").css("visibility", "hidden");
        }
    });
    
    // Changing compare plan inputs 
    $("#AnytimeRateInputBox").val(37.73);
    $("#ControlledRateInputBox").val(18.69);
    $("#SupplyChargeInputBox").val(88.43);
    $("#FeedInTariffInputBox").val(Math.abs(-14.20));

    var selectedPlan = "aglStd";
    $("#StandardPlanSelector").change(function() {
        var companyPrices = {
            "aglStd": [37.73, 18.69, 88.43, -14.20],
            // "dmo": [35.76, 18.92, 110.0, -8.0],
            "alintaStd": [37.44, 18.73, 91.30, -9.50],
            "originStd": [37.98, 18.07, 85.71, -8.00],
            "energyAusStd": [37.97, 18.07, 85.80, -11.50],
            "simplyEnergyStd": [36.00, 22.19, 98.63, -15.00],
            "custom": [35.3, 20.00, 110.0, -8.0]
        };
        var inputAnytime = $("#AnytimeRateInputBox");
        var inputControlled = $("#ControlledRateInputBox");
        var inputSupply = $("#SupplyChargeInputBox");
        var inputSolar = $("#FeedInTariffInputBox");
        
        selectedPlan = this.value;
        var selectedValues = companyPrices[selectedPlan];

        inputAnytime.val(selectedValues[0]);
        inputControlled.val(selectedValues[1]);
        inputSupply.val(selectedValues[2]);
        inputSolar.val(Math.abs(selectedValues[3]));
    });

    

    // Disable Inputs
    let nonPostcodeInputs = $("#UseDetailForm input:not(#PostcodeInputBox, #ResidentialRadioButton, #BusinessRadioButton, .suggest), #SavingsControlPanel input, #CurrentPlanControlPanel input, #CurrentPlanControlPanel select");
    
    nonPostcodeInputs.prop('disabled', true);
    $("#AnnualUseInputBox").addClass("behind");

    // style radio buttons to reflect the fact that they are disabled

    // $("div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), div.radioButtonsSize3, div.radioButtonsSize4").addClass("childDisabled");
    //$("input[type='radio']:not(#ResidentialRadioButton, #BusinessRadioButton)").addClass("radioDisabled");
    //$("label[for='1Person']").addClass("labelDisabled");

    $("#UseDetailForm div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), #UseDetailForm div.radioButtonsSize3, #UseDetailForm div.radioButtonsSize4, #SavingsControlPanel div.radioButtonsSize2, #SavingsControlPanel div.radioButtonsSize3, #SavingsControlPanel div.radioButtonsSize4, #CurrentPlanControlPanel div.radioButtonsSize2, #CurrentPlanControlPanel div.radioButtonsSize3, #CurrentPlanControlPanel div.radioButtonsSize4").addClass("childDisabled");

    // $("#AnnualUseInputs").addClass("disable");

    $("#UseDetailForm div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), #UseDetailForm div.radioButtonsSize3, #UseDetailForm div.radioButtonsSize4, #SavingsControlPanel div.radioButtonsSize2, #SavingsControlPanel div.radioButtonsSize3, #SavingsControlPanel div.radioButtonsSize4, #CurrentPlanControlPanel div.radioButtonsSize2, #CurrentPlanControlPanel div.radioButtonsSize3, #CurrentPlanControlPanel div.radioButtonsSize4").on("click",function(){
        $("#PostcodeModal").modal('show');
        $("#PostcodeInputBox").addClass("is-invalid");
    });

    // testing csv fetch
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState == 4 && xhr.status == 200){
            var x = parseCsv(xhr.responseText);
            postcodes = createDict(x);
            autocomplete(document.getElementById("PostcodeInputBox"), Object.keys(postcodes));
        }
    }
    xhr.open('GET', 'postcodes.csv');
    xhr.send();

    // Useful in detecting whether a browser is Chrome:
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent
    if (navigator.userAgent.indexOf("Chrome") != -1) {
        browserIsChrome();
    }
});


// Function to show the correct message on loading the page, Specifically for the file upload.
function showUploadResultMessage(message) {
    var errorMsgsMap = new Map();

    errorMsgsMap.set("SuccessfulUpload", new uploadResultMessage("SuccessfulUpload", "Your upload was successful!", "assets/successImg.svg"));
    errorMsgsMap.set("TooBig_2MB", new uploadResultMessage("TooBig_2MB", "Your file was too big! Keep the size under 2MB.", "assets/errorImg.svg"));
    errorMsgsMap.set("Error", new uploadResultMessage("Error", "Something went wrong!<br>Please try again or contact us via email.<br><a class=' underlineLink' href='mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site'>save@ioenergy.com.au</a>", "assets/errorImg.svg"));
    errorMsgsMap.set("TypeError", new uploadResultMessage("TypeError", "Wrong Type. Please only upload .jpg .png or .pdf.", "assets/errorImg.svg"));
    errorMsgsMap.set("Unfinished", new uploadResultMessage("Unfinished", "You didn't give us enough information!", "assets/errorImg.svg"));
    errorMsgsMap.set("Uncaught", new uploadResultMessage("Uncaught", "Ooops, Something went wrong! Please try again!", "assets/errorImg.svg"));

    if (errorMsgsMap.has(message)){
        $('#BillUploadForm').hide();
        $('#UploadStatusMsgContainer').show();

        $('#UploadStatusMsgImg').attr("src", errorMsgsMap.get(message).img);
        $('#UploadStatusMsgText').append(errorMsgsMap.get(message).msg);

        $('#UploadBillModal').modal('show');

    } else {
        $('#BillUploadForm').show();
        $('#UploadStatusMsgContainer').hide();
    }
}
// Class to hold details about each msg
class uploadResultMessage {
    constructor(msgCode, msg, img) {
      this.msgCode = msgCode;
      this.msg = msg;
      this.img = img;
    }
}



function createDict(arr){
    // console.log(arr);
    var dict = {};
    arr.forEach(function(entry) {
        // console.log(entry);

        if (entry[0] in dict) {
            dict[entry[0]][0].push(entry[1]);
        } else {
            dict[entry[0]] = [[entry[1]],entry[2]];
        }
    });
    
    // console.log(dict);

    // arr.forEach(function(entry) {
    //     console.log(entry);
    //   });
    // let dict = {}
    // let head = arr[0];
    // let tail = arr.slice(1);
    // tail.forEach(e => {
    //     dict[(e[0].toString())] = [e[1], e[2]];
    // });
    // console.log(dict);
    return dict;
}

function browserIsChrome() {
    // Add Chrome class to radio buttons of different sizes
    // Useful in adding classes to tags in Jquery:
    // https://api.jquery.com/addClass/
    $(".radioButtonsSize2").addClass("chromeRadioButtons2");
    $(".radioButtonsSize3").addClass("chromeRadioButtons3");
    $(".radioButtonsSize4").addClass("chromeRadioButtons4");
}

$( function(){
    $("#AnytimeRateInputBox").focus(function(){
        $("#StandardPlanSelector").val("custom");
    });
    $("#ControlledRateInputBox").focus(function(){
        $("#StandardPlanSelector").val("custom");
    });
    $("#SupplyChargeInputBox").focus(function(){
        $("#StandardPlanSelector").val("custom");
    });
    $("#FeedInTariffInputBox").focus(function(){
        $("#StandardPlanSelector").val("custom");
    });
});

/* Making detailed rates modal appear depending on which plan link is pressed */
function checkPlan(planId) {
    var modal = document.getElementById("DetailedRatesModal");
    var modalImg = $("#DetailRatesImage");

    if (planId == "SparkDetailRates") {
        // console.log("Spark");
        // modalImg.hide();
        modalImg.attr("src", "assets/IOPriceMaps-Spark.jpg");
        $('#DetailedRatesNote').hide();
        /* modalImg.hide(); */
        // modalImg.src = "/assets/IOPriceMaps-Spark.jpg";
    } else if (planId == "MillenniumDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Millennium.jpg");
        $('#DetailedRatesNote').show();

    } else if (planId == "LightningDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Lightning.jpg");
        $('#DetailedRatesNote').show();

    } else if (planId == "JupiterDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Jupiter.jpg");
        $('#DetailedRatesNote').show();

    } else if (planId == "PulseDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Pulse.jpg");
        $('#DetailedRatesNote').hide();

    } else if (planId == "PulseDetailRates+") {
        modalImg.attr("src", "assets/IOPriceMaps-Pulse+.jpg");
        $('#DetailedRatesNote').hide();

    }
    
    $("#DetailedRatesModal").modal('show');
}

// Disclaimer Modal 'I Understand Button'

function acknowledgeDisclaimer() {

    $('#DisclaimerModal').modal('hide');
}

//Join mailing list function 
$('#ActiveCampaignModal').hide();
function joinMailingList() {
    $('#MailingListButton').hide();
    $('#ActiveCampaignModal').fadeIn(500);
}

// Display Related JS
$( function() {
    // Make Handles Display Values
    var peopleHandle = $( "#people-handle" );
    var solarHandle = $( "#solar-handle" );
    var controlHandle = $( "#control-handle" );
    var offPeakHandle = $( "#off-peak-handle" );
    var billHandle = $( "#bill-handle" );
    $( "#people-slider" ).slider({
        min:1,
        max: 5,
        value: 2,
        create: function() {
            peopleHandle.text( $( this ).slider( "value" ) );
        },
        slide: function( event, ui ) {
            peopleHandle.text( ui.value );
        }
    });
    $( "#solar-slider" ).slider({
        min: 0,
        max: 1000,
        value: 0,
        create: function() {
            solarHandle.text( $( this ).slider("value"));
        },
        slide: function( event, ui) {
            solarHandle.text(ui.value);
        }
    });
    $( "#control-slider" ).slider({
        min: 0,
        max: 5000,
        value: 0,
        create: function() {
            controlHandle.text( $( this ).slider("value"));
        },
        slide: function( event, ui) {
            controlHandle.text(ui.value);
        }
    });
    $( "#off-peak-slider" ).slider({
        min: 0,
        max: 100,
        value: 22,
        create: function() {
            offPeakHandle.text( $( this ).slider("value") + "%");
        },
        slide: function( event, ui) {
            offPeakHandle.text(ui.value + "%");
        },
        change: function( event, ui) {
            offPeakHandle.text(ui.value + "%");
        }
    });
    $( "#bill-slider" ).slider({
        min: 0,
        max: 100,
        value: 22.5,
        create: function() {
            billHandle.text( $( this ).slider("value"));
        },
        slide: function( event, ui) {
            billHandle.text(ui.value);
        }
    });

    $("#off-peak-slider").slider("disable");

    var peopleStartPos = $("#people-slider");
    var peopleEndPos = '';
    var solarStartPos = $("#solar-slider");
    var solarEndPos = '';
    var controlStartPos = $("#control-slider");
    var controlEndPos = '';
    var offPeakStartPos = $("#off-peak-slider");
    var offPeakEndPos = '';
    var billStartPos = $("#bill-slider");
    var billEndPos = '';

} );

/* Function to check postcode input */
function checkPostcode(postcode) {
    // console.log("in check postcode function");
    if (postcode.toString().length >= 3) {
        // console.log("Full postcode : " + postcode.toString());
        var pc = parseInt(postcode);
        // SA Postcode
        // console.log("OH MY GOD");
        // console.log(postcodes);
        if (postcode in postcodes){
            switch(postcodes[postcode][1]){
                case "TAS":
                case "WA":
                case "NT":
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                case "NSW":
                case "ACT":
                case "VIC":
                case "QLD":
                    $("#PostcodeModal1").modal('show');
                    break;
                case "SA":
                    break;
                default:
                    break;
            }
            // enable non-postcode inputs
            $("#UseDetailForm input:not(#PostcodeInputBox, #ResidentialRadioButton, #BusinessRadioButton, .suggest), #SavingsControlPanel input, #CurrentPlanControlPanel input, #CurrentPlanControlPanel select").prop('disabled', false);

            // re-style radio buttons to reflect the fact that they are now enabled
            $("#UseDetailForm div.radioButtonsSize2:not(#TypeOfPlansRadioButtons),#UseDetailForm div.radioButtonsSize3, #UseDetailForm div.radioButtonsSize4, #SavingsControlPanel div.radioButtonsSize2, #SavingsControlPanel div.radioButtonsSize3, #SavingsControlPanel div.radioButtonsSize4, #CurrentPlanControlPanel div.radioButtonsSize2, #CurrentPlanControlPanel div.radioButtonsSize3, #CurrentPlanControlPanel div.radioButtonsSize4").removeClass("childDisabled");
            $("input[type='radio']:not(#ResidentialRadioButton, #BusinessRadioButton)").removeClass("radioDisabled");
            $("label[for='1Person']").removeClass("labelDisabled");

            // re-enable jqueryUI slider
            $("#off-peak-slider").slider("enable");

            // remove invalid styling from postcode box
            $("#PostcodeInputBox").removeClass("is-invalid");
            
            // enable annual use inputs
            $("#AnnualUseInputs").removeClass("disable");
            $("#AnnualUseInputBox").removeClass("behind");

            // remove on click modal for inputs
            $("div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), div.radioButtonsSize3, div.radioButtonsSize4, #off-peak-slider, #AnnualUseInputs").off("click");        
        }

/*         if (5000 <= pc && pc <= 5999) {
            console.log("South Australian postcode");
            $("#PostcodeContainer").popover('hide');
        } 
        // NSW, ACT, VIC, QLD Postcode
        else if (2000 <= pc && pc <= 4999) {
            console.log("Austalian postcode");

            $("#PostcodeModal1").modal('show');
        } 
        // WA, TAS, NT Postcode
        else if ((6000 <= pc && pc <= 7999) || (800 <= pc && pc <= 899)) {
            $('#ActiveCampaignModal').hide();
            $("#PostcodeModal2").modal('show');
        }
 */

    } else {
        console.log("invalid postcode");

        // disable non-postcode inputs
        $("#UseDetailForm input:not(#PostcodeInputBox, #ResidentialRadioButton, #BusinessRadioButton, .suggest), #SavingsControlPanel input, #CurrentPlanControlPanel input, #CurrentPlanControlPanel select").prop('disabled', true);

        // style radio buttons to be disabled
        $("#UseDetailForm div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), #UseDetailForm div.radioButtonsSize3, #UseDetailForm div.radioButtonsSize4, #SavingsControlPanel div.radioButtonsSize2, #SavingsControlPanel div.radioButtonsSize3, #SavingsControlPanel div.radioButtonsSize4, #CurrentPlanControlPanel div.radioButtonsSize2, #CurrentPlanControlPanel div.radioButtonsSize3, #CurrentPlanControlPanel div.radioButtonsSize4").addClass("childDisabled");        $("input[type='radio']:not(#ResidentialRadioButton, #BusinessRadioButton)").addClass("radioDisabled");

        // disable jqueryUI slider
        $("#off-peak-slider").slider("disable");

        $("#AnnualUseInputs").addClass("disable");
        $("#AnnualUseInputBox").addClass("behind");

        // re enable on click
        $("div.radioButtonsSize2:not(#TypeOfPlansRadioButtons), div.radioButtonsSize3, div.radioButtonsSize4, #off-peak-slider, #AnnualUseInputs").on("click",function(){
            $("#PostcodeModal").modal('show');
        });

    }
}

/* Checking if any of the input fields are changed and initialising values */
function initialiseInputs() {
    /* Check if the postcode input has been changed */
    var postcode = 0;
    $("#PostcodeInputBox").change(function() {
        postcode = this.value;
        checkPostcode(postcode);
        // console.log("identified postcode inputbox change");
    });

    /* Check if residential or business buttons have changed */
    var businessButton = 0;
    $("input[type=radio][name=TypeOfPlansRadioButtons]").change(function() {
        $('#NoControlledLoad').prop('checked', true);
        $("#ControlledInput").fadeOut(500);
        usageControlledAmount = 0;

        if (this.value == 1) {
            usageSliderAmount = 20000;
            businessButton = 1;
            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        } else {
            usageSliderAmount = 2;
            businessButton = 0;
            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        }
    });

    /* Initialising the usage amount and checking if no of people or custom input have changed */
    var usageSliderAmount = 2;
    $("input[type=radio][name=NoOfPeopleRadioButtons]").change(function() {
        usageSliderAmount = this.value;
        

        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

        checkCompare();
    });
    // Annual use in first section
    $("#AnnualUseInputBox").change(function() {
        usageSliderAmount = this.value;

        if (businessButton == 1) {
            $(".percOffText").hide();
            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

            // Displaying modal if over 160,000
            if (160000 <= usageSliderAmount) {
                $("#BusinessOver").modal('show');
            }
        } else {
            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        }

                    
        checkCompare();
    });
    // Annual use in last years bill section
    $("#AnnualUseInputBox2").change(function() {
        usageSliderAmount = this.value;
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

        // Displaying modal if over 160,000
        if (160000 <= usageSliderAmount) {
            $("#BusinessOver").modal('show');
        }

        checkCompare();
    });

    /* Initialising the controlled usage and updating usage onchange*/
    var usageControlledAmount = 0;
    $("input[type=radio][name=ControlledLoadRadioButtons]").change(function() {

        if(this.value == 0) {
            usageControlledAmount = 0;

            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        } else {
            usageControlledAmount = 1;

            if (businessButton == 1) {
                $("#StandardPlanSelector").val('custom');

                checkCompare();

                displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
            } else {
                displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
            }
        }
    });
    // Controlled use in first section
    $("#ControlledLoadInputBox").change(function() {
        usageControlledAmount = this.value;
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

        checkCompare();
    });
    // Controlled use in last years bill section
    $("#ControlledLoadInputBox2").change(function() {
        usageControlledAmount = this.value;
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });
    
    /* Initialising the solar feed slider and updating it on change */
    var solarFeedAmount = 0;
    $("#SolarFeedInInputBox").change(function() {
        solarFeedAmount = this.value;
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

        checkCompare();
    });

    /* Checking if the Billing Interval is changed and updating accordingly */
    var billing = 1;
    $("input[type=radio][name=BillingCycleRadioButtons]").change(function() {
        billing = paymentType(this.value);

        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });

    /* Checking if the off Peak use is changed and updating values accordingly */
    var offPeak = 0.2246;
    $("#off-peak-slider").on("slidestop", function(event, ui){
        offPeak = (ui.value*0.01);
        
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

        checkCompare();
    });

    /* Checking if the last year's bill input box has been changed */
    
    var lastYearsBill = 0;
    $("input[type=radio][name=CompareToPlanRadioButtons]").change(function() {
        // Comparing to last years bill
        if (this.value == 1) {
            customerHasBill = 0;
            $("#LastYearsBillInputBox").val(null);

            $(".customerSavings").css("visibility", "hidden");
            $(".marketOfferSavings").css("visibility", "hidden");
            $(".annualBill").css("visibility", "hidden");
            
            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        } 
        // Comparing to competing plans
        else if (this.value == 2) {
            lastYearsBill = 2;

            if (businessButton == 1) {
                $("#StandardPlanSelector").val('custom');

                displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

            } else {
                displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
            }


            $(".customerSavings").css("visibility", "visible");
            $(".marketOfferSavings").css("visibility", "visible");
            $(".annualBill").css("visibility", "visible");

            // Hide Jupiter Plan Related Comparisons
            $("#JupiterComparison").css("visibility", "hidden");
            $("#JupiterAnnualBill").css("visibility", "hidden");
        } 
        // Comparing to default 
        else {
            lastYearsBill = 0;
            offPeak = 0.2246;
            billing = 1;
            solarFeed = 0;

            if (businessButton == 1) {
                usageSliderAmount = 20000;
                
            } else {
                usageSliderAmount = 2
            }

            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);

            $(".customerSavings").css("visibility", "visible");
            $(".marketOfferSavings").css("visibility", "visible");
            $(".annualBill:not(#JupiterAnnualBill)").css("visibility", "visible");
        }
    });
    var customerHasBill = 0
    $("#LastYearsBillInputBox").change(function() {
        lastYearsBill = this.value;
        if (lastYearsBill > 0) {
            customerHasBill = 1;
            $(".customerSavings").css("visibility", "visible");
            $(".marketOfferSavings").css("visibility", "visible");
            $(".annualBill:not(#JupiterAnnualBill)").css("visibility", "visible");
            $("#PulseSavings").css("visibility", "hidden");
            $("#PulseMarketOfferSaving").css("visibility", "hidden");

            displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
        } else {
            $(".customerSavings").css("visibility", "hidden");
            $(".marketOfferSavings").css("visibility", "hidden");
            $(".annualBill").css("visibility", "hidden");
            $("#PulseSavings").css("visibility", "hidden");
            $("#PulseMarketOfferSaving").css("visibility", "hidden");

            customerHasBill = 0;
        }
    });

    /* Checking if the plan to compare to has been changed */
    $("#StandardPlanSelector").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });

    /*Checking if any of the custom compare inputs have been changed */
    $("#AnytimeRateInputBox").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });
    $("#ControlledRateInputBox").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });
    $("#SupplyChargeInputBox").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });
    $("#FeedInTariffInputBox").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });

    // Check if the discount input box has been changed
    $("#DiscountInputBox").change(function() {
        displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeedAmount, offPeak, billing, lastYearsBill);
    });
}

/* Function which updates the General Usage and Controlled Usage every time the inputs are changed for either of them*/
function updateUsage(usageSliderAmount, usageControlledAmount) {
    var usageGeneral = 0;
    var usageControlled = 0;
    
    // If they are using the default values but have a controlled load
    if (usageControlledAmount == 1) {
        if (usageSliderAmount == 1) {
            usageGeneral = 3100 * 1.05;
            usageControlled = 3100 * 0.45;
        } else if (usageSliderAmount == 2) {
            usageGeneral = 4000 * 1.05;
            usageControlled = 4000 * 0.45;
        } else if (usageSliderAmount == 3) {
            usageGeneral = 4745 * 1.05;
            usageControlled = 4745 * 0.45;
        } else if (usageSliderAmount == 4 || usageSliderAmount == 5) {
            usageGeneral = 6168.5 * 1.05;
            usageControlled = 6168.5 * 0.45;
        } else {
            usageGeneral = usageSliderAmount * 1.05;
            usageControlled = usageSliderAmount * 0.45;
        }
    } 
    // If they have a custom controlled load
    else if (usageControlledAmount > 1) {
        if (usageSliderAmount == 1) {
            usageGeneral = 3100 * 1.05;
            usageControlled = usageControlledAmount;
        } else if (usageSliderAmount == 2) {
            usageGeneral = 4000 * 1.05;
            usageControlled = usageControlledAmount;
        } else if (usageSliderAmount == 3) {
            usageGeneral = 4745 * 1.05;
            usageControlled = usageControlledAmount;
        } else if (usageSliderAmount == 4 || usageSliderAmount == 5) {
            usageGeneral = 6168.5 * 1.05;
            usageControlled = usageControlledAmount;
        } else {
            usageGeneral = usageSliderAmount * 1.05;
            usageControlled = usageControlledAmount;
        }
    }
    // If they dont have a controlled load
    else {
        if (usageSliderAmount == 1) {
            usageGeneral = 3100;      
        } else if (usageSliderAmount == 2) {
            usageGeneral = 4000;
        } else if (usageSliderAmount == 3) {
            usageGeneral = 4745;
        } else if (usageSliderAmount == 4 || usageSliderAmount == 5) {
            usageGeneral = 6168.5;
        } else {
            usageGeneral = usageSliderAmount;
        }
    }
    return [usageGeneral, usageControlled];
}

/* Function which handles the billing intervals the customer puts in */
function paymentType(payment) {
    if (payment == "0") {
        return 1;
    } else if (payment == "1") {
        return 0.95;
    } else if (payment == "2") {
        return 0.9;
    }
}

/* Function that rounds up to 5 */
function round5(x) {
    return Math.ceil(x/5)*5;
}

/* Function which checks if customer is comparing to last years bill and prevents it changing to competiting plans when they are */
function checkCompare() {
    var radioValue = $("input[name=CompareToPlanRadioButtons]:checked").val();
    // If it's already selected don't change when inputs change
    if (radioValue == 1) {
    } 
    // If it isn't selected change to competing plans comparison
    else {
        $('#CompetingPlans').prop('checked', true);

        $("input[type=radio][name=CompareToPlanRadioButtons]:checked").change();
    }
}

/* Spark Monthly Payments */
function calculateSpark(usageSliderAmount, usageControlledAmount, solarFeed, billingInterval) {
    var values = updateUsage(usageSliderAmount, usageControlledAmount);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRate = usageGeneral;
    var costGeneral = 30/100 * usageRate;
    var clRate = usageControlled;
    var costCL = 18/100 * clRate;
    var costDailySupply = 110/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;

    var monthlyPaymentSpa = (costGeneral + costCL + costDailySupply + costSolarFit)/12 * billingInterval;
    return monthlyPaymentSpa;
}

/* Millennium Monthly Payments */
function calculateMillennium(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval) {
    var values = updateUsage(usageSliderAmount, usageControlledAmount);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRatePeak = usageGeneral*(0.7858*(1-offPeak));
    var costUsageRatePeak = 36/100 * usageRatePeak;
    var usageRateEcon = usageGeneral*(0.2142*(1-offPeak));
    var costRateEcon = 18/100 * usageRateEcon;
    var usageHappyHour = usageGeneral * offPeak;
    var costHappyHour = 12/100 * usageHappyHour;
    var clRate = usageControlled;
    var costCL = 12/100 * clRate;
    var costDailySupply = (146.16)/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;

    var monthlyPaymentMil = (costUsageRatePeak + costRateEcon + costHappyHour + costCL + costDailySupply + costSolarFit)/12 * billingInterval;
    return monthlyPaymentMil;
}

/* Lightning Monthly Payments */
function calculateLightning(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval) {
    var values = updateUsage(usageSliderAmount, usageControlledAmount);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRatePeakNovMar = usageGeneral * (0.10653*(1-offPeak));
    var costUsageRatePeakNovMar = 125/100 * usageRatePeakNovMar;
    var usageRatePeakAprOct = usageGeneral * (0.15089*(1-offPeak));
    var costUsageRatePeakAprOct = 36/100 * usageRatePeakAprOct;
    var usageRatePrem = usageGeneral * (0.52837*(1-offPeak));
    var costRatePrem = 20/100 * usageRatePrem;
    var usageRateEcon = usageGeneral * (0.21421*(1-offPeak));
    var costRateEcon = 12/100 * usageRateEcon;
    var usageHappyHour = usageGeneral * offPeak;
    var costHappyHour = 8/100 * usageHappyHour;
    var clRate = usageControlled;
    var costCL = 8/100 * clRate;
    var costDailySupply = (146.16)/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit= (-4)/100 * solarFitUsage;

    var monthlyPaymentLig = (costUsageRatePeakNovMar + costUsageRatePeakAprOct + costRatePrem + costRateEcon + costHappyHour + costCL + costDailySupply + costSolarFit)/12 * billingInterval;
    return monthlyPaymentLig;
}

/* Calculate Pulse payments */
function calculatePulse(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval) {
    var values = updateUsage(usageSliderAmount, usageControlledAmount);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRate = usageGeneral;
    var costUsageRate = 31/100 * usageRate;
    var clRate = usageControlled;
    var costCL = 19/100 * clRate;
    var costDailySupply = 160/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;

    var monthlyPaymentPul = (costUsageRate + costCL + costDailySupply + costSolarFit)/12 * billingInterval;
    return monthlyPaymentPul;
}

function calculateComparison(planGeneralUse, usageGeneral, planControlUse, usageControlled, planSupplyCharge, noOfDays, planSolarFit, solarFeed) {
    var costGeneral = planGeneralUse/100 * usageGeneral;
    var costControlled = planControlUse/100 * usageControlled;
    var costDailySupply = planSupplyCharge/100 * noOfDays;
    var costSolarFit = planSolarFit/100 * solarFeed;
    
    var comparisonValue = (costGeneral + costControlled + costDailySupply + costSolarFit)/12
    return comparisonValue;    
}

function calculateSavingReimagined(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval, lastYearsBill) {
    // Get the sliders for energy comparison
    // var lastBillSlider = document.getElementById("BillSlider");
    var thisYearBillMenu = document.getElementById("StandardPlanSelector");

    // Using a 2D array for the various prices for different energy companies (each inner array is of the 
    // form [general usage rate, controlled load rate, daily supply rate, solar FiT]):
    var companyPrices = [
        // Other
        [35.3, 20.00, 110.0, -8.0],
        // AGL
        [37.73, 18.69, 88.43, -14.20],
        // Origin
        [37.98, 18.07, 85.71, -8.00],
        // EnergyAus
        [37.97, 18.07, 85.80, -11.50],
        // Simply
        [36.00, 22.19, 98.63, -15.00],
        // Alinta
        [37.44, 18.73, 91.30, -9.50],
        // Default
        [35.76, 18.92, 110.0, -8.0]
    ];

    // Finding the general usage and usage controlled
    var values = updateUsage(usageSliderAmount, usageControlledAmount);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    /* If they're using the default values */
    if (lastYearsBill == 0) {
        comparisonValue = calculateComparison(companyPrices[6][0], usageGeneral, companyPrices[6][1], usageControlled, companyPrices[6][2], 365, companyPrices[6][3], solarFeed);
    } 
    /* If they're comparing to competing plans */
    else if (lastYearsBill == 2) {
        switch (thisYearBillMenu.value) {
            case "aglStd":
                // AGL Standard
                comparisonValue = calculateComparison(companyPrices[1][0], usageGeneral, companyPrices[1][1], usageControlled, companyPrices[1][2], 365, companyPrices[1][3], solarFeed);
                break;
            case "originStd":
                // Origin Standard
                comparisonValue = calculateComparison(companyPrices[2][0], usageGeneral, companyPrices[2][1], usageControlled, companyPrices[2][2], 365, companyPrices[2][3], solarFeed);
                break;
            case "energyAusStd":
                // EnergyAustralia Standard
                comparisonValue = calculateComparison(companyPrices[3][0], usageGeneral, companyPrices[3][1], usageControlled, companyPrices[3][2], 365, companyPrices[3][3], solarFeed);
                break;
            case "simplyEnergyStd":
                // SimplyEnergy Standard
                comparisonValue = calculateComparison(companyPrices[4][0], usageGeneral, companyPrices[4][1], usageControlled, companyPrices[4][2], 365, companyPrices[4][3], solarFeed);
                break;
            case "alintaStd":
                // Alinta Standard
                comparisonValue = calculateComparison(companyPrices[5][0], usageGeneral, companyPrices[5][1], usageControlled, companyPrices[5][2], 365, companyPrices[5][3], solarFeed);
                break;
            case "custom":
                    // The customer manually inputs the values
                    var anytimeRate = document.getElementById("AnytimeRateInputBox").value;
                    var controlRate = document.getElementById("ControlledRateInputBox").value;
                    var supplyRate = document.getElementById("SupplyChargeInputBox").value;
                    var solarFit = -document.getElementById("FeedInTariffInputBox").value;

                comparisonValue = calculateComparison(anytimeRate, usageGeneral, controlRate, usageControlled, supplyRate, 365, solarFit, solarFeed);
                break;
        }
    } 
    /* If they're comparing to last years bill */
    else {
        comparisonValue = lastYearsBill/12;
    } 
    // console.log("comparison value:" + comparisonValue)

    // Apply discount
    var discountPerc = getDiscount();
    if (discountPerc != null) {
        console.log(discountPerc);
        comparisonValue = comparisonValue * (1 - (discountPerc / 100));
    }

    /* Spark Plan Values */
    var monthlyPaymentSpa = calculateSpark(usageSliderAmount, usageControlledAmount, solarFeed, billingInterval);
    var yearlyPaymentSpa = monthlyPaymentSpa*12;
    var yearlySavingSpa = (comparisonValue - monthlyPaymentSpa)*12;
    var dmoSpa = 1 - (monthlyPaymentSpa / comparisonValue);

    /* Millennium Plan Values */
    var monthlyPaymentMil = calculateMillennium(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval);
    var yearlyPaymentMil = monthlyPaymentMil*12;
    var yearlySavingMil = (comparisonValue - monthlyPaymentMil)*12;
    var dmoMil = 1 - (monthlyPaymentMil / comparisonValue);

    /* Lightning Plan Values */
    var monthlyPaymentLig = calculateLightning(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval);
    var yearlyPaymentLig = monthlyPaymentLig*12;
    var yearlySavingLig = (comparisonValue - monthlyPaymentLig)*12;
    var dmoLig = 1 - (monthlyPaymentLig / comparisonValue);

    /* Pulse Plan Values */
    var comparisonPul = 8305/12;
    var monthlyPaymentPul = calculatePulse(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval);
    var yearlyPaymentPul = monthlyPaymentPul*12;
    var yearlySavingsPul = (comparisonPul - monthlyPaymentPul)*12;
    var dmoPul = 1 - (monthlyPaymentPul / comparisonPul);

    /* Initialising the arrays of values */
    var yearlyPaymentArr = [yearlyPaymentSpa, yearlyPaymentMil, yearlyPaymentLig, yearlyPaymentPul];
    var yearlySavingsArr = [yearlySavingSpa, yearlySavingMil, yearlySavingLig, yearlySavingsPul];
    var dmoArr = [100*dmoSpa, 100*dmoMil, 100*dmoLig, 100*dmoPul];

    return [yearlyPaymentArr, yearlySavingsArr, dmoArr];
}

function displaySavingsReimagined(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval, lastYearsBill) {
    // Yearly savings at index 0, % off DMO at index 1
    var calculateSavingsArray = calculateSavingReimagined(usageSliderAmount, usageControlledAmount, solarFeed, offPeak, billingInterval, lastYearsBill);

    // if (window.getComputedStyle(document.getElementById("SparkSavings")).visibility == "hidden") {
        if (calculateSavingsArray[1][0] < 0) {
            document.getElementById("SparkSavings").innerHTML = "Spend &#36;" + 
                (-Math.ceil(calculateSavingsArray[1][0])) + " extra pa";
            // document.getElementById("SparkSavings").style.color = "#ED1515";
        } else {
            document.getElementById("SparkSavings").innerHTML = "Save &#36;" + 
                Math.ceil(calculateSavingsArray[1][0]) + " pa";
            // document.getElementById("SparkSavings").style.color = "black";
        }
    // }
    // if (window.getComputedStyle(document.getElementById("MilleniumSavings")).visibility == "hidden") {
        if (calculateSavingsArray[1][1] < 0) {
            document.getElementById("MillenniumSavings").innerHTML = "Spend &#36;" + 
                (-Math.ceil(calculateSavingsArray[1][1])) + " extra pa";
            // document.getElementById("MillenniumSavings").style.color = "#ED1515";
        } else {
            document.getElementById("MillenniumSavings").innerHTML = "Save &#36;" + 
                Math.ceil(calculateSavingsArray[1][1]) + " pa";
            // document.getElementById("MillenniumSavings").style.color = "black";
        }
    // }
    // if (document.getElementById("LightningSavings").hidden == false) {
        if (calculateSavingsArray[1][2] < 0) {
            document.getElementById("LightningSavings").innerHTML = "Spend &#36;" + 
                (-Math.ceil(calculateSavingsArray[1][2])) + " extra pa";
            // document.getElementById("LightningSavings").style.color = "#ED1515";
        } else {
            document.getElementById("LightningSavings").innerHTML = "Save &#36;" + 
                Math.ceil(calculateSavingsArray[1][2]) + " pa";
            // document.getElementById("LightningSavings").style.color = "black";
        }
    // }
    // if (document.getElementById("PulseSavings").hidden == false) {
        if (calculateSavingsArray[1][3] < 0) {
            document.getElementById("PulseSavings").innerHTML = "Spend &#36;" + 
                (-Math.ceil(calculateSavingsArray[1][3])) + " extra pa";
            // document.getElementById("LightningSavings").style.color = "#ED1515";
        } else {
            document.getElementById("PulseSavings").innerHTML = "Save &#36;" + 
                Math.ceil(calculateSavingsArray[1][3]) + " pa";
            // document.getElementById("LightningSavings").style.color = "black";
        }
    // }

    // Change the 'percentage off' text (e.g. change from DMO)
    updatePercOffDisplay(lastYearsBill);

    /* Monthly Savings */
    document.getElementById("SparkAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][0]) + "pa incl. GST";
    document.getElementById("MillenniumAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][1]) + "pa incl. GST";
    document.getElementById("LightningAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][2]) + "pa incl. GST";
    document.getElementById("PulseAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][3]) + "pa incl. GST";
    
    var percOffMoreThanText = document.getElementsByClassName("percOffMoreThanText");
    if (calculateSavingsArray[2][0] < 0) {
        document.getElementById("SparkDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][0])) + "%";
        percOffMoreThanText[0].innerHTML = "more";
    } else {
        document.getElementById("SparkDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][0]) + "%";
        percOffMoreThanText[0].innerHTML = "less";
    }
    if (calculateSavingsArray[2][1] < 0) {
        document.getElementById("MillenniumDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][1])) + "%";
        percOffMoreThanText[1].innerHTML = "more";
    } else {
        document.getElementById("MillenniumDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][1]) + "%";
        percOffMoreThanText[1].innerHTML = "less";
    }
    if (calculateSavingsArray[2][2] < 0) {
        document.getElementById("LightningDMOBold").innerHTML = 
            -(Math.ceil(calculateSavingsArray[2][2])) + "%";
        percOffMoreThanText[2].innerHTML = "more";
    } else {
        document.getElementById("LightningDMOBold").innerHTML = 
            Math.ceil(calculateSavingsArray[2][2]) + "%";
        percOffMoreThanText[2].innerHTML = "less";
    } if (calculateSavingsArray[2][3] < 0) {
        document.getElementById("PulseDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][3])) + "%";
        percOffMoreThanText[3].innerHTML = "more";
    } else {
        document.getElementById("PulseDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][3]) + "%";
        percOffMoreThanText[3].innerHTML = "less";
    }
}

function updatePercOffDisplay(lastBillValue) {
    var percOffText = document.getElementsByClassName("percOffText");
    var thisYearBillMenu = document.getElementById("StandardPlanSelector");
    // If they're comparing to dmo
    if (lastBillValue == 0) {
        if (parseInt($("input[name=NoOfPeopleRadioButtons]:checked").val()) == 2 || $("input[type=radio][name=TypeOfPlansRadioButtons]").val() == 1) {
            for (var i = 0; i < percOffText.length; i++) {
                percOffText[i].innerHTML = "than the Reference Price";
            }
        } else {
            for (var i = 0; i < percOffText.length; i++) {
                percOffText[i].innerHTML = "than your previous bill";
            }
        }
    }
    // If they're comparing to competing plans
    else if (lastBillValue == 2) {
        switch (thisYearBillMenu.value) {
            case "aglStd":
                // AGL Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than AGL Standard";
                }
                break;
            case "originStd":
                // Origin Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than Origin Standard";
                }
                break;
            case "energyAusStd":
                // EnergyAustralia Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than EnergyAustralia Standard";
                }
                break;
            case "simplyEnergyStd":
                // SimplyEnergy Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than SimplyEnergy Standard";
                }
                break;
            case "alintaStd":
                // Alinta Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than Alinta Standard";
                }
                break;
            case "custom":
                // The customer manually inputs the values
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than your previous bill";
                }
                break;
        }
    }
    else {
        for (var i = 0; i < 4; i++) {
            percOffText[i].innerHTML = "than your previous bill";
        }
    }
}

//Smooth Scroll to Our Plans Section
function scrollTooOurPlans() {
    document.querySelector('#OurPlans').scrollIntoView({ behavior: 'smooth' });
}

//Smooth Scroll to FAQs
function scrollToFAQ() {
    const id = 'FAQSection';
    const yOffset = -60; 
    const element = document.getElementById(id);
    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({top: y, behavior: 'smooth'});
    // document.querySelector('#FAQSection').scrollIntoView({ behavior: 'smooth' });
}

function parseCsv(input){
    let csvData = [];
    let lineBreak = input.split("\n");
    lineBreak.forEach(e => {
        csvData.push(e.split(","));
    });
    // console.table(csvData);
    return csvData;
}

function autocomplete(input, arr){
    var currentFocus;
    input.addEventListener("input", function(e){
        var a, b, val = this.value;
        closeAllLists();
        if (!val) {return false;}
        a = document.createElement("div");
        a.setAttribute("id", this.id + "-autocomplete-items");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        var suggest = []
        for (var i = 0; i < arr.length; i++){
            if (suggest.length <= 5){
                if (arr[i].substr(0, val.length) == val) {
                    // console.log(arr[i]);
                    suggest.push(arr[i]);
                    b = document.createElement("div");

                    b.innerHTML = 
                    "<p class='autocomplete-row'>"+ 
                        //Postcode itself
                        "<span class='pcNum'><strong>" + arr[i].substr(0, val.length) + "</strong>" + arr[i].substr(val.length) + "</span>" + " - " +

                        //Associated Suburbs
                        toTitleCase(postcodes[arr[i]][0].join(', ').replace(/['"]+/g, '')) +
                    "</p>" + 
                    // Clickability 
                    "<input type='hidden' class='suggest' value='" + arr[i] + "'>";


                    b.addEventListener("click", function(e){
                        input.value = this.getElementsByClassName("suggest")[0].value;
                        checkPostcode(input.value);
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            } else {
                break;
            }
        }
    });
    input.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40){
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38){
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13){
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++){
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(element) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++){
            if (element != x[i] && element != input) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

// Function to redirect customers to signup on Energy Locals
// Branches to do gtag tracking and to display disclaimer modals for time of use.
function signup(plan) {

    // All the information needed to create EL Link
    // EL link itself is stored in a global variable elURL
    var postcode = $("#PostcodeInputBox").val();
    var resOrBus = $("input[name=TypeOfPlansRadioButtons]:checked").val();
    var name = '';
    var value = '';
    var offeringcode = '';

    // Check type of plan
    if (resOrBus == 0) {
        name = 'Residential';
        value = 'RESIDENT';
    } else {
        name = 'Business';
        value = 'COMPANY';
    }

    // Check the plancode itself
    if (plan == 'spark') {
        offeringcode = 'RESI_IO_SPRK'
    } else if (plan == 'millennium') {
        offeringcode = 'RESI_IO_MIL'
    } else if (plan == 'lightning') {
        offeringcode = 'RESI_IO_LGHT'
    } else if (plan == 'jupiter') {
        offeringcode = 'RESI_IO_JUPT'
    } else if (plan == 'pulse') {
        offeringcode = 'SME_IO_PLS'
    } else if (plan == 'pulseplus') {
        offeringcode = 'SME_IO_PLSP'
    }

    // Check if there is a postcode if not stop the user
    var pc = parseInt(postcode);
    if (!postcode) {
        $("#PostcodeModal").modal('show')

    } else if (pc < 5000 || pc > 5999) { //if we are not in SA
        var pc = parseInt(postcode);
        
        // Get the state the postcode is in
        if (postcode in postcodes){
            switch(postcodes[postcode][1]){
                case "TAS":
                case "WA":
                case "NT":
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                case "NSW":
                case "ACT":
                case "VIC":
                case "QLD":
                    $("#PostcodeModal1").modal('show');
                    break;
                case "SA":
                    break;
                default:
                    break;
            }
        }

    } else {

        // Create the elURL for this configuration
        elURL = 'https://energylocals.com.au/sign-up/?Postcode=' + postcode + '&ctName=' + name + '&ctValue=' + value +'&BrandCode=IOENERGY_5064_X&OfferingCode=' + offeringcode;
        // console.log(url);

        // For time of use plans show modal and only proceed when the modal is acknowledged
        if (plan == 'millennium' || plan == 'lightning' || plan == 'jupiter') {
            console.log("Disclaimer for Time OF Use Plans");
            $("#TOUDisclaimerModal").modal('show');

        } else { // For non time of use plans call appropriate gtag and redirect to appropriate el page
            
             // Facebook Pixel
            fbq('track', 'Purchase', {
                content_name: plan + '_Plan'
            });

            return sendGtagRedirectToELpage(elURL, plansGtags[plan]);
        }
    }
}

// When the modal is acknowledged redirect
function acknowledgeTOUDisclaimer() {
    var plan = "";

    // Get the plan name from the elURL so we can get the right gtag
    if (elURL.includes('RESI_IO_SPRK')) {
        plan = 'spark';

    } else if (elURL.includes('RESI_IO_MIL')) {
        plan = 'millennium';

    } else if (elURL.includes('RESI_IO_LGHT')) {
        plan = 'jupiter';

    } else if (elURL.includes('RESI_IO_JUPT')) {
        plan = 'pulse';

    } else if (elURL.includes('SME_IO_PLS')) {
        plan = 'pulse';

    } else if (elURL.includes('SME_IO_PLSP')) {
        plan = 'pulseplus';

    }

    // Facebook Pixel
    fbq('track', 'Purchase', {
        content_name: plan + '_Plan'
    });

    // Send Gtag and redirect
    sendGtagRedirectToELpage(elURL, plansGtags[plan]);
}


// Send tracking information to google, gtagcode is unique to each plan
function sendGtagRedirectToELpage(url, gtagCode) {

    // See if there is a postcode
    var postcode = $("#PostcodeInputBox").val();
    if (!postcode) {

    } else {

        // On successful cookie send prep the EL page to redirect too
        var callback = function () { 
            if (typeof(url) != 'undefined') { window.location = url; } 
        }; 

        // Send tracking cookie to google 
        gtag('event', 'conversion', {
             'send_to': gtagCode, 
             'event_callback': callback 
            }); 

        // Unsure if this is nessasarcy
        return false;
    }
}


// Checks the Type of device accessing the page and from there chooses if a phone number is better or a mobile number is.
function mobileCheck(id, subject) {

    // Identify if mobile device
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    
    
    // Replace spaces with %20 so it is read as a space
    subject = String(subject).replace(/ /gi, "%20");
    console.log(subject);

    // Choose appropriate contact type
    if (check == true) {
        $("#"+id).prop("href", "tel:1300313463;");
    } else {
        $("#"+id).prop("href", "mailto:hello@ioenergy.com.au?subject=" + subject);
    }
    // mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site;
};



//Opens billUploadModal so we can collect the bill and relevent info.

/* function autocomplete(input, arr){
    var currentFocus;
    console.log("we in autocomplete");
    // function runs when input into the text field
    input.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val){ return false; }
        currentFocus = -1;
        a = document.createElement("div");
        a.setAttribute("id", this.id + "autocomplete-items");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < arr.length; i++){
            console.log("bababooie");
            if (arr[i].substr(0, val.length) == val) {
                console.log("item matched");
                b = document.createElement("div");
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length) + " | " + postcodes[arr[i]];
                b.innerHTML += "<input type='hidden' class='suggest' value='" + arr[i] + "'>";
                b.addEventListener("click", function(e){
                    input.value = this.getElementsByClassName("suggest")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    input.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40){
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38){
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13){
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++){
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(element) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++){
            if (element != x[i] && element != input) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
} */

// Get and return any energy plan discount entered in the input box
function getDiscount() {
    var discountPerc = parseInt($("#DiscountInputBox").val());
    // Check whether the value entered is a valud number and is between 0 and 100
    if (isNaN(discountPerc) == false && 0 <= discountPerc && discountPerc <= 100) {
        return discountPerc;
    }
    return null;
}
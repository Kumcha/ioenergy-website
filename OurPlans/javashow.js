
// TODO: Fix Page Scaling for Mobiles

var postcodes = {};

const State = {
    ACT: Symbol('ACT'),
    NSW: Symbol('NSW'),
    VIC: Symbol('VIC'),
    TAS: Symbol('TAS'),
    SA:  Symbol('SA'),
    WA:  Symbol('WA'),
    NT:  Symbol('NT'),
    QLD: Symbol('QLD')
};

/* Change what value displays in annual input and controlled load when amount of people, controlled load, and business are changed*/
var noOfPeopleDefaults = {
    1 : [3100, 3255, 1395],
    2 : [4000, 4200, 1800],
    3 : [4745, 4982.25, 2135.25],
    4 : [6168.5, 6476.925, 2775.825],
    5 : [6168.5, 6476.925, 2775.825],
    6 : [20000, 20000, 100]
};

/* Plan Class */

class Plan {
    // --- class methods ---
    constructor(postcode, state, business){
        this._postcode = postcode;
        this._state = state;
        this._business = business;
        this._people = (business) ? null : 2;
        this._usage = noOfPeopleDefaults[this._people][0];
        this._specifiedUsage = 0;
        this._controlled = 0;
        this._controlledAmount = 0;
        this._solarFeed = 0;
        this._offPeak = 0.2246;
        this._billingPeriod = 1.00;
        this._pastProvider = "AGL";
        this._comparison = 0;
        this._calculator = false;
    }

    get calculator(){
        return this._calculator;
    }

    get comparison() {
        return this._comparison;
    }
    
    get useSpecified(){
        return (this._specifiedUsage > 0);
    }

    get state() {
        return this._state;
    }

    get business() {
        return this._business;
    }

    get postcode(){
        return this._postcode;
    }

    get controlled(){
        return this._controlled;
    }

    get solarFeed(){
        return this._solarFeed;
    }

    get offPeak(){
        return this._offPeak;
    }

    get billingPeriod(){
        return this._billingPeriod;
    }

    get usage(){
        if (this._specifiedUsage > 0){ return this._specifiedUsage; }
        else { return this._usage; }
    }

    get people(){
        return this._people;
    }

    get controlled(){
        return this._controlled;
    }

    get controlledAmount(){
        return this._controlledAmount;
    }

    get pastProvider(){
        return this._pastProvider;
    }

    set state(s) {
        this._state = s;
    }

    set postcode(pc){
        this._postcode = pc;
    }

    set usage(u){
        console.log("USAGE WAS CHANGED");
        this._usage = u;
    }

    set comparison(c){
        this._comparison = c;
    }

    set people(noPeople){
        this._people = noPeople;
        this.usage = noOfPeopleDefaults[noPeople][this._controlled];
        if (this._controlled == 1) { this.controlledAmount = noOfPeopleDefaults[noPeople][2] }
    }

    set business(bool){
        this._business = bool;
        if (this._business){
            this.people = 6;
            this.usage = noOfPeopleDefaults[6][0];
            if (this.controlled){
                this.controlledAmount = noOfPeopleDefaults[6][2];
            }
        } else {
            this.people = 2;
            this.usage = noOfPeopleDefaults[this.people][this.controlled];
            if (this.controlled){
                this.controlledAmount = noOfPeopleDefaults[this.people][this.controlled];
            }
        }
    }

    set controlled(c){
        this._controlled = c;
        this.people = this._people;
        if (c == 1){ this.controlledAmount = noOfPeopleDefaults[this._people][2] }
        else {this.controlledAmount = 0}
    }

    set solarFeed(sf){
        this._solarFeed = sf;
    }

    set offPeak(op){
        this._offPeak = op;
    }

    set billingPeriod(bp){
        //console.log("billing period discount " + parseFloat(bp));
        this._billingPeriod = parseFloat(bp);
    }

    set specifiedUsage(usage){
        this._specifiedUsage = usage;
    }

    set controlledAmount(ca){
        this._controlledAmount = ca;
    }

    set pastProvider(pp){
        switch (pp){
            case "AGL":
            case "EnergyAus":
            case "other":
            case "Origin":
            case "Simply":
            case "Alinta":
            case "DMO":
                this._pastProvider = pp;
                break;
            default:
                //console.log("invalid provider");
                break;
        }
    }

    set calculator(c){
        this._calculator = c;
    }
} 

var plan = new Plan();

$( document ).ready(function() {
    // clear and reset inputs to default values
    initInputs();

    // Into slide button press to move to postcode slide
    $("button#introNextBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#postcode-slide"));
    });
    // Postcode slide back button press to move back to intro slide
    $("button#postcodeBackBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#intro-slide"));
    });
    // disable postcode slide next until a postcode is entered
    $("button#postcodeNextBtn").prop('disabled', true);
    $("button#postcodeNextBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#property-slide"));
    });

    // Set isBusiness based upon button pushed on the property type slide
    $("button#residentialBtn").click(function(event){
        plan.business = false;
        hideActiveSlide();
        showSlide($("#compare-slide"));
    });
    $("button#businessBtn").click(function(event){
        plan.business = true;
        hideActiveSlide();
        showSlide($("#compare-slide"));
    });

    // move to relevant slide depending on selected comparison method
    $("button#uploadBillBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#upload-slide"));
    });
    $("button#useCalcBtn").click(function(event){
        hideActiveSlide();
        plan.calculator = true;
        initInputs();
        if (plan.business) {
            $("#NoOfPeopleRadioButtonsContainer").hide();
            updateInputs();
        } 
        else {$("#NoOfPeopleRadioButtonsContainer").show();}
        showSlide($("#consumption-slide"));
    });
    $("button#seePlansBtn").click(function(event){
        hideActiveSlide();
        plan.calculator = false;
        if (plan.business){
            showSlide($("#business-plans-slide"));
        } else {
            showSlide($("#residential-plans-slide"));
        }
    });

    // calculate savings and prices and navigate once calculator is complete
    $("button#consumptionNextBtn").click(function(event){
        hideActiveSlide();
        //console.log(calculateSavings());
        displaySavings();
        if (plan.business){
            showSlide($("#business-plans-slide"));
        } else {
            showSlide($("#residential-plans-slide"));
        }
    })


    // Setup back buttons for all slides
    $("button#propertyBackBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#postcode-slide"));
    });
    $("button#compareBackBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#property-slide"));
    });
    $("button#uploadBackBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#compare-slide"));
    });
    $("button#consumptionBackBtn").click(function(event){
        hideActiveSlide();
        showSlide($("#compare-slide"));
    });
    $("button#residentialPlansBackBtn").click(function(event){
        hideActiveSlide();
        if (plan.calculator){
            showSlide($("#consumption-slide"));
        } else {
            showSlide($("#compare-slide"));
        }
    });
    $("button#businessPlansBackBtn").click(function(event){
        hideActiveSlide();
        if (plan.calculator){
            showSlide($("#consumption-slide"));
        } else {
            showSlide($("#compare-slide"));
        }
    });

    // Setup next button for calculator slide
    $("button#consumptionNextBtn").click(function(event){
        hideActiveSlide();
        if (plan.business) showSlide($("#business-plans-slide"));
        else showSlide($("#residential-plans-slide"));
    });

    // Fetch postcodes
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState == 4 && xhr.status == 200){
            var x = parseCsv(xhr.responseText);
            postcodes = createDict(x);
            autocomplete(document.getElementById("PostcodeInputBox"), Object.keys(postcodes));
        }
    }
    xhr.open('GET', 'postcodes.csv');
    xhr.send();

    // add handlers to all controls
    addCalculatorEvents();
});

function hideActiveSlide(){
    $(".slide--active").removeClass("slide--active");
    updateInputs();
}

function showSlide(target){
    target.addClass("slide--active");
}

// POSTCODES

/* Function to check postcode input */
function checkPostcode(postcode) {
    // console.log("in check postcode function");
    if (postcode.toString().length >= 3) {
        // console.log("Full postcode : " + postcode.toString());
        var pc = parseInt(postcode);
        if (postcode in postcodes){
            $("button#postcodeNextBtn").prop('disabled',false);
            switch(postcodes[postcode][1]){
                // redirect to energylocals website
                case "QLD":
                   plan = new Plan(pc, State.QLD, false);
                    $("#PostcodeModal1").modal('show');
                    break;
                case "NSW":
                    plan = new Plan(pc, State.NSW, false);
                    $("#PostcodeModal1").modal('show');
                    break;
                case "ACT":
                    plan = new Plan(pc, State.ACT, false);
                    $("#PostcodeModal1").modal('show');
                    break;
                case "VIC":
                    plan = new Plan(pc, State.VIC, false);
                    $("#PostcodeModal1").modal('show');
                    break;
         
                // join waitlist if not above states
                case "WA":
                    plan = new Plan(pc, State.WA, false);
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                case "TAS":
                    plan = new Plan(pc, State.TAS, false);
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                case "NT":
                    plan = new Plan(pc, State.NT, false);
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                // proceed through calculator
                case "SA":
                    plan = new Plan(pc, State.SA, false);
                    break;
                default:
                    break;
            }
        }
    } else {
        $("button#postcodeNextBtn").prop('disabled', true);
    }
}

// Show waitlist

function showWaitlist(){
    hideActiveSlide();
    showSlide($("#waitlist-slide"));
}

// parse the postcode CSV file

function parseCsv(input){
    let csvData = [];
    let lineBreak = input.split("\n");
    lineBreak.forEach(e => {
        csvData.push(e.split(","));
    });
    // console.table(csvData);
    return csvData;
}

// create dictionary from postcodes array

function createDict(arr){
    var dict = {};
    arr.forEach(function(entry) {

        if (entry[0] in dict) {
            dict[entry[0]][0].push(entry[1]);
        } else {
            dict[entry[0]] = [[entry[1]],entry[2]];
        }
    });
    
    return dict;
}

function autocomplete(input, arr){
    var currentFocus;
    input.addEventListener("input", function(e){
        var a, b, val = this.value;
        closeAllLists();
        if (!val) {return false;}
        a = document.createElement("div");
        a.setAttribute("id", this.id + "-autocomplete-items");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        var suggest = []
        for (var i = 0; i < arr.length; i++){
            if (suggest.length <= 5){
                if (arr[i].substr(0, val.length) == val) {
                    // console.log(arr[i]);
                    suggest.push(arr[i]);
                    b = document.createElement("div");

                    b.innerHTML = 
                    "<p class='autocomplete-row'>"+ 
                        //Postcode itself
                        "<span class='pcNum'><strong>" + arr[i].substr(0, val.length) + "</strong>" + arr[i].substr(val.length) + "</span>" + " - " +

                        //Associated Suburbs
                        toTitleCase(postcodes[arr[i]][0].join(', ').replace(/['"]+/g, '')) +
                    "</p>" + 
                    // Clickability 
                    "<input type='hidden' class='suggest' value='" + arr[i] + "'>";


                    b.addEventListener("click", function(e){
                        input.value = this.getElementsByClassName("suggest")[0].value;
                        checkPostcode(input.value);
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            } else {
                break;
            }
        }
    });
    input.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40){
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38){
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13){
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    input.onchange = function() {
        $("button#postcodeNextBtn").prop('disabled', true);
    };
    
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++){
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(element) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++){
            if (element != x[i] && element != input) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

function initInputs(){
    // clear postcode box
    $("#PostcodeInputBox").val('');
    // select default value for number of people radio buttons
    $("input#2People").prop('checked', true);
    // clear annual consumption
    $("#AnnualUseInputBox").val(4000);
    // select no controlled load
    $("input#NoControlledLoad").prop('checked', true);
    // clear controlled load input box
    $("input#ControlledLoadInputBox").val('');
    // hiding the controlled load input box
    $("#ControlledInput").hide();
    // select no solar
    $("input#NoSolarFeed").prop('checked', true);
    // clear solar input box
    $("input#SolarFeedInInputBox").val('');
    // select no home battery 
    $("input#NoBattery").prop('checked', true);
    // select no interest in home battery
    $("input#NoInterest").prop('checked', true);
    // select monthly billing
    $("input#Monthly").prop('checked', true);
    // select reference price for comparison
    $("#ReferencePrice").prop('checked', true);
    // Hide the comparison inputs
    $("#BillKnown").hide();
    $("#BillUnknown").hide();
    // Hide solar input box
    $("#SolarInput").hide();
    //Hide Lightning and Jupiter signup links
    $("#SignupLight").hide();
    $("#SignupJupiter").hide();

    /* Check if the postcode input has been changed */
    var postcode = 0;
    $("#PostcodeInputBox").change(function() {
        postcode = this.value;
        checkPostcode(postcode);
    });
    // off peak slider 
    var offPeakHandle = $("#off-peak-handle");
    $("#off-peak-slider").slider({
        min: 0,
        max: 100,
        value: 22.46,
        create: function() {
            offPeakHandle.text( $(this).slider("value") + "%");
        },
        slide: function(event, ui){
            offPeakHandle.text(ui.value + "%");
        },
        change: function(event, ui){
            offPeakHandle.text(ui.value + "%");
            if (ui.value == 22){
                plan.offPeak = 0.2246;
            } else {
                plan.offPeak = ui.value/100;
            }
        },
        stop: function(event, ui){
            checkCompare()
        }
    });

    /*Check if business is selected */
    if (plan.business == 1) {
        $("#off-peak-sliderLabel").hide()
        $("#off-peak-slider").hide()
        $("#StandardPlanSelector").val('other');
    }
}


function updateInputs() {
    $("#AnnualUseInputBox").val(round5(plan.usage));
    $("#ControlledLoadInputBox").val(round5(plan.controlledAmount));

    var inputAnytime = $("#AnytimeRateInputBox");
    var inputControlled = $("#ControlledRateInputBox");
    var inputSupply = $("#SupplyChargeInputBox");
    var inputSolar = $("#FeedInTariffInputBox");
    
    plan.pastProvider = this.value;
    //console.log("pastProvider " + plan.pastProvider);
    var selectedValues = companyPrices[plan.pastProvider];

    inputAnytime.val(selectedValues[0]);
    inputControlled.val(selectedValues[1]);
    inputSupply.val(selectedValues[2]);
    inputSolar.val(Math.abs(selectedValues[3]));
}

// handling calculator inputs
function addCalculatorEvents(){
    updateInputs();
    // handle changes in number of people
    $("input[type=radio][name=NoOfPeopleRadioButtons]").change(function() {
        //console.log("You changed the number of people.");
        plan.specifiedUsage = 0;
        //console.log("This.value is " + this.value);
        plan.people = this.value;
        
        //$("input#AnnualUseInputBox").val(plan.usage);
        // Display the appropriate defaults
        //displayInputs (plan.people, plan.controlled, plan.business);
        updateInputs();
        checkCompare();
    });
    // handle changes in custom usage
    $("#AnnualUseInputBox").change(function() {
        if (160000 <= this.value) {
            $("#BusinessOver").modal('show');
        }
        
        //console.log("Annual use changed");
        plan.specifiedUsage = this.value;
        checkCompare();
    });
    // handle controlled usage changes
    $("input[type=radio][name=ControlledLoadRadioButtons]").change(function() {
        plan.controlled = Number(this.value);
        updateInputs();
        // if they dont have a controlled load
        if (this.value == 0) { 
            $("#ControlledInput").fadeOut(500);

             // If business display the appropriate defaults
             if (plan.business == 1) {
                updateInputs();
                $(".percOffText").hide();
            } else {
                updateInputs();
            }
            
        }
        // if they DO have a controlled load
        else {
            $("#ControlledInput").fadeIn(500);

            // If business display the appropriate defaults
            if (plan.business == 1) {
                updateInputs();
                $("#StandardPlanSelector").val("other");
                checkCompare();
                //Changing the text inside the pulse plan
                $(".percOffText").hide();
            } else {
                updateInputs();
            }

        }

        updateInputs();
    });
    $("#ControlledLoadInputBox").change(function() {
        plan.controlledAmount = this.value;
        checkCompare();
    });
    // Adding solar input box if they have one
    solarButton = 0;
    $("input[type=radio][name=SolarFeedRadioButtons]").change(function() {
        solarButton = this.value;
        // If they HAVE solar
        if (solarButton == 1) {
            $("#SolarInput").delay(500).fadeIn();
        }
        // If they DONT HAVE solar 
        else {
            $("#SolarInput").fadeOut(500);
            plan.solarFeed = 0;
        }
    })
    // handle solar feed input box changes
    $("#SolarFeedInInputBox").change(function() {
        plan.solarFeed = this.value;
        checkCompare();
    });
    // Making the InterestBattery question disappear if they have a battery
    homeBattery = 0;
    $("input[type=radio][name=HomeBatteryRadioButtons]").change(function() {
        homeBattery = this.value;

        // If they HAVE home battery
        if (homeBattery == 1) {
            $("#BatteryInterestLabel").fadeOut();
            $("#BatteryInterestRadioButtons").fadeOut();

            $("#SignupLight").show();
            $("#SignupJupiter").show();
            $("#BookACallLight").hide();
            $("#BookACallJupiter").hide();
        }
        // If they DONT HAVE home battery 
        else {
            $("#BatteryInterestLabel").delay(500).fadeIn();
            $("#BatteryInterestRadioButtons").delay(500).fadeIn();

            $("#SignupLight").hide();
            $("#SignupJupiter").hide();
            $("#BookACallLight").show();
            $("#BookACallJupiter").show();
        }
    })
    // Making Modal appear if they're interested in a home battery
    batteryInterest = 0;
    $("input[type=radio][name=BatteryInterestRadioButtons]").change(function() {
        batteryInterest = this.value;
        // If they ARE interested in home battery
        if (batteryInterest == 1) {
            $("#BookACallModal").modal('show');
        }
        // If they are NOT interested 
        else {
        }
    })
    $("input[type=radio][name=CompareToPlanRadioButtons]").change(function() {
        updateInputs()
        plan.comparison = this.value;
        // If they know their bill
        if (this.value == 1) {
            $("#DefaultNote").fadeOut(500);
            $("#BillUnknown").fadeOut(500);
            $("#BillKnown").delay(500).fadeIn(500);
            $("#JupiterComparison").css("visibility", "hidden");
            //console.log("They know their bill");
        }
        // If they are comparing to competing plans
        else if (this.value == 2) {
            $("#DefaultNote").fadeOut(500);
            $("#BillKnown").fadeOut(500);
            $("#BillUnknown").delay(500).fadeIn(500);
            // $("#JupiterComparison").css("visibility", "visible");
            //console.log("They are comparing using competing plans");

            if (plan.business == 1) {
                $(".percOffText").hide();
                $(".percOffText").hide();
                // $("#StandardPlanSelector").val('custom');
                //console.log("And they are a business");
            } 
        } 
        // If they are using default values
        else {
            updateInputs();
            $("#BillKnown").fadeOut(500);
            $("#BillUnknown").fadeOut(500);
            $("#DefaultNote").delay(500).fadeIn(500);
            if (plan.business == 0){
                // Changing the values and buttons back to default 
                plan.people = 2;
                updateInputs();
                $('#2People').prop('checked', true);
                $('#SolarFeedInInputBox').val(null);
                $( "#off-peak-slider" ).slider({
                    value: 22.46
                });
                $("#Monthly").prop('checked',true);
                if (plan.controlled == 1) {
                    updateInputs();
                } else {
                    controlledUseButton = 0
                    updateInputs();
                }
            }
            // BUSINESS
            else {
                //$("#AnnualUseInputBox").val(20000);

                $(".percOffText").show();

                $('#NoControlledLoad').prop('checked', true);
                $("#ControlledInput").fadeOut(500);

                //displaySavingsReimagined(20000, 0, 0, 0.2246, 1, 0)
            } 

            $("#JupiterComparison").css("visibility", "hidden");
        }
    });
    $("input[type=radio][name=BillingCycleRadioButtons]").change(function() {
        //console.log(this.value);
        plan.billingPeriod = paymentType(this.value);
        //console.log("plan.billingPeriod " + plan.billingPeriod);
    });

    // Set Values for different providers plans
    $("#StandardPlanSelector").change(function() {
        var inputAnytime = $("#AnytimeRateInputBox");
        var inputControlled = $("#ControlledRateInputBox");
        var inputSupply = $("#SupplyChargeInputBox");
        var inputSolar = $("#FeedInTariffInputBox");
        
        plan.pastProvider = this.value;
        //console.log("pastProvider " + plan.pastProvider);
        var selectedValues = companyPrices[plan.pastProvider];

        inputAnytime.val(selectedValues[0]);
        inputControlled.val(selectedValues[1]);
        inputSupply.val(selectedValues[2]);
        inputSolar.val(Math.abs(selectedValues[3]));
    });
}

// Updates all the inputs
function displayInputs(selectedPeople, controlledUseButton, businessButton) {
    var inputGeneral = plan.usage;
    var inputControlled = plan.controlled;

    var selectedValues = noOfPeopleDefaults[plan.people];

    /* If they don't have a controlled use */
    if (plan.controlled == 0) {
        inputGeneral.val(round5(selectedValues[0]));
        inputControlled.val(0);
    } 
    /* If they do have a controlled use */
    else {
        if (plan.business == 1) {
            inputGeneral.val(round5(selectedValues[0]));
            inputControlled.val(round5(selectedValues[1]));
        } else {
            inputGeneral.val(round5(selectedValues[1]));
            inputControlled.val(round5(selectedValues[2]));
        }
    }        
}

/* Function which checks if customer is comparing to last years bill and prevents it changing to competiting plans when they are */
function checkCompare() {
    var radioValue = $("input[name=CompareToPlanRadioButtons]:checked").val();
    // If it's already selected don't change when inputs change
    if (radioValue == 1) {
    } 
    // If it isn't selected change to competing plans comparison
    else {
        $('#CompetingPlans').prop('checked', true);

        $("input[type=radio][name=CompareToPlanRadioButtons]:checked").change();
    }
}

/* Function that rounds up to 5 */
function round5(x) {
    return Math.ceil(x/5)*5;
}

// return discount 
function paymentType(bp){
    switch (bp){
        default:
        case "0":
            return 1.00;
        case "1":
            return parseFloat("0.95");
        case "2":
            return parseFloat("0.9");
    }
}


// Calculation Stuff
const companyPrices = {
    // Other
    "other" : [35.3, 20.00, 110.0, -8.0],
    // AGL
    "AGL" : [37.73, 18.69, 88.43, -14.20],
    // Origin
    "Origin" : [37.98, 18.07, 85.71, -8.00],
    // EnergyAus
    "EnergyAus" :[37.97, 18.07, 85.80, -11.50],
    // Simply
    "Simply" :[36.00, 22.19, 98.63, -15.00],
    // Alinta
    "Alinta" :[37.44, 18.73, 91.30, -9.50],
    // Default
    "DMO" :[35.76, 18.92, 110.0, -8.0]
};

function calculateSavings(){
    // set default market offer as comparison
    var oldPlan = companyPrices[plan._pastProvider];
    //console.log("oldPlan is " + oldPlan[0]);
    // evaluate comparison value for DMO
    var comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
    //console.log("comparison is " + comparison);

    /* If they're using the default values */
    if (plan.comparison == 0) {
        comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
    } 
    /* If they're comparing to competing plans */
    else if (plan.comparison == 2) {
        //console.log("BOOO");
        switch (plan.pastProvider) {
            case "AGL":
                // AGL Standard
                comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
                break;
            case "Origin":
                // Origin Standard
                comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
                break;
            case "EnergyAus":
                // EnergyAustralia Standard
                comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
                break;
            case "Simply":
                // SimplyEnergy Standard
                comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
                break;
            case "Alinta":
                // Alinta Standard
                comparison = calculateComparison(oldPlan[0], plan.usage, oldPlan[1], plan.controlledAmount, oldPlan[2], 365, oldPlan[3], plan.solarFeed);
                break;
            case "other":
                    // The customer manually inputs the values
                    var anytimeRate = document.getElementById("AnytimeRateInputBox").value;
                    var controlRate = document.getElementById("ControlledRateInputBox").value;
                    var supplyRate = document.getElementById("SupplyChargeInputBox").value;
                    var solarFit = -document.getElementById("FeedInTariffInputBox").value;

                comparison = calculateComparison(anytimeRate, plan.usage, controlRate, plan.controlledAmount, supplyRate, 365, solarFit, plan.solarFeed);
                break;
        }
    } 
    /* If they're comparing to last years bill */
    else {
        comparison = plan.comparison/12;
    } 
    // console.log("comparison value:" + comparison)

    // Apply discount
    var discountPerc = paymentType(plan.billingPeriod);
    if (discountPerc != null) {
        //console.log(discountPerc);
        comparison = comparison * discountPerc;
    }

    if (!plan.business){
        /* Spark Plan Values */
        var monthlyPaymentSpark = calculateSpark(plan.usage, plan.controlledAmount, plan.solarFeed, plan.billingPeriod);
        //console.log("monthlyPaymentSpark is " + monthlyPaymentSpark);
        var yearlyPaymentSpark = monthlyPaymentSpark*12;
        var yearlySavingSpark = (comparison - monthlyPaymentSpark)*12;
        var dmoSpark = 1 - (monthlyPaymentSpark / comparison);

        /* Millennium Plan Values */
        var monthlyPaymentMillennium = calculateMillennium(plan.usage, plan.controlledAmount, plan.solarFeed, plan.offPeak, plan.billingPeriod);
        var yearlyPaymentMillennium = monthlyPaymentMillennium*12;
        var yearlySavingMillennium = (comparison - monthlyPaymentMillennium)*12;
        var dmoMillennium = 1 - (monthlyPaymentMillennium / comparison);

        /* Lightning Plan Values */
        var monthlyPaymentLightning = calculateLightning(plan.usage, plan.controlledAmount, plan.solarFeed, plan.offPeak, plan.billingPeriod);
        var yearlyPaymentLightning = monthlyPaymentLightning*12;
        var yearlySavingLightning = (comparison - monthlyPaymentLightning)*12;
        var dmoLightning = 1 - (monthlyPaymentLightning / comparison);
    }
    else {
        /* Pulse Plan Values */
        var comparisonPulse = 8305/12;
        var monthlyPaymentPulse = calculatePulse(plan.usage, plan.controlledAmount, plan.solarFeed, plan.offPeak, plan.billingPeriod);
        var yearlyPaymentPulse = monthlyPaymentPulse*12;
        var yearlySavingsPulse = (comparisonPulse - monthlyPaymentPulse)*12;
        var dmoPulse = 1 - (monthlyPaymentPulse / comparisonPulse);
    }
    /* Initialising the arrays of values */
    var yearlyPaymentArray = [yearlyPaymentSpark, yearlyPaymentMillennium, yearlyPaymentLightning, yearlyPaymentPulse];
    var yearlySavingsArray = [yearlySavingSpark, yearlySavingMillennium, yearlySavingLightning, yearlySavingsPulse];
    var dmoArray = [100*dmoSpark, 100*dmoMillennium, 100*dmoLightning, 100*dmoPulse];

    updatePercOffDisplay(plan.comparison);

    return [yearlyPaymentArray, yearlySavingsArray, dmoArray];
}

function updatePercOffDisplay(lastBillValue) {
    var percOffText = document.getElementsByClassName("percOffText");
    var thisYearBillMenu = document.getElementById("StandardPlanSelector");
    // If they're comparing to dmo
    if (lastBillValue == 0) {
        if (parseInt($("input[name=NoOfPeopleRadioButtons]:checked").val()) == 2 || plan.business == 1) {
            for (var i = 0; i < percOffText.length; i++) {
                percOffText[i].innerHTML = "than the Reference Price";
            }
        } else {
            for (var i = 0; i < percOffText.length; i++) {
                percOffText[i].innerHTML = "than your previous bill";
            }
        }
    }
    // If they're comparing to competing plans
    else if (lastBillValue == 2) {
        switch (thisYearBillMenu.value) {
            case "AGL":
                // AGL Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than AGL Standard";
                }
                break;
            case "Origin":
                // Origin Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than Origin Standard";
                }
                break;
            case "EnergyAus":
                // EnergyAustralia Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than EnergyAustralia Standard";
                }
                break;
            case "Simply":
                // SimplyEnergy Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than SimplyEnergy Standard";
                }
                break;
            case "Alinta":
                // Alinta Standard
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than Alinta Standard";
                }
                break;
            case "other":
                // The customer manually inputs the values
                for (var i = 0; i < 4; i++) {
                    percOffText[i].innerHTML = "than your previous bill";
                }
                break;
        }
    }
    else {
        for (var i = 0; i < 4; i++) {
            percOffText[i].innerHTML = "than your previous bill";
        }
    }
}

function displaySavings(){
    // array with yearly savings at index 0 and % off dmo at index 1
    var calculateSavingsArray = calculateSavings();

    // set the savings text on each plan card to be representative of the savings calculations

    if (calculateSavingsArray[1][0] < 0) {
        document.getElementById("SparkSavings").innerHTML = "Spend &#36;" + 
            (-Math.ceil(calculateSavingsArray[1][0])) + " extra pa";
        // document.getElementById("SparkSavings").style.color = "#ED1515";
    } else {
        document.getElementById("SparkSavings").innerHTML = "Save &#36;" + 
            Math.ceil(calculateSavingsArray[1][0]) + " pa";
        // document.getElementById("SparkSavings").style.color = "black";
    }

    if (calculateSavingsArray[1][1] < 0) {
        document.getElementById("MillenniumSavings").innerHTML = "Spend &#36;" + 
            (-Math.ceil(calculateSavingsArray[1][1])) + " extra pa";
        // document.getElementById("MillenniumSavings").style.color = "#ED1515";
    } else {
        document.getElementById("MillenniumSavings").innerHTML = "Save &#36;" + 
            Math.ceil(calculateSavingsArray[1][1]) + " pa";
        // document.getElementById("MillenniumSavings").style.color = "black";
    }

    if (calculateSavingsArray[1][2] < 0) {
        document.getElementById("LightningSavings").innerHTML = "Spend &#36;" + 
            (-Math.ceil(calculateSavingsArray[1][2])) + " extra pa";
        // document.getElementById("LightningSavings").style.color = "#ED1515";
    } else {
        document.getElementById("LightningSavings").innerHTML = "Save &#36;" + 
            Math.ceil(calculateSavingsArray[1][2]) + " pa";
        // document.getElementById("LightningSavings").style.color = "black";
    }

    if (calculateSavingsArray[1][3] < 0) {
        document.getElementById("PulseSavings").innerHTML = "Spend &#36;" + 
            (-Math.ceil(calculateSavingsArray[1][3])) + " extra pa";
        // document.getElementById("LightningSavings").style.color = "#ED1515";
    } else {
        document.getElementById("PulseSavings").innerHTML = "Save &#36;" + 
            Math.ceil(calculateSavingsArray[1][3]) + " pa";
        // document.getElementById("LightningSavings").style.color = "black";
    }

    // updatePercOffDisplay();

    /* Monthly Savings */
    document.getElementById("SparkAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][0]) + "pa incl. GST";
    document.getElementById("MillenniumAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][1]) + "pa incl. GST";
    document.getElementById("LightningAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][2]) + "pa incl. GST";
    document.getElementById("PulseAnnualBill").innerHTML = "&#36;" + Math.ceil(calculateSavingsArray[0][3]) + "pa incl. GST";

    var percOffMoreThanText = document.getElementsByClassName("percOffMoreThanText");
    if (calculateSavingsArray[2][0] < 0) {
        document.getElementById("SparkDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][0])) + "%";
        percOffMoreThanText[0].innerHTML = "more";
    } else {
        document.getElementById("SparkDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][0]) + "%";
        percOffMoreThanText[0].innerHTML = "less";
    }
    if (calculateSavingsArray[2][1] < 0) {
        document.getElementById("MillenniumDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][1])) + "%";
        percOffMoreThanText[1].innerHTML = "more";
    } else {
        document.getElementById("MillenniumDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][1]) + "%";
        percOffMoreThanText[1].innerHTML = "less";
    }
    if (calculateSavingsArray[2][2] < 0) {
        document.getElementById("LightningDMOBold").innerHTML = 
            -(Math.ceil(calculateSavingsArray[2][2])) + "%";
        percOffMoreThanText[2].innerHTML = "more";
    } else {
        document.getElementById("LightningDMOBold").innerHTML = 
            Math.ceil(calculateSavingsArray[2][2]) + "%";
        percOffMoreThanText[2].innerHTML = "less";
    } if (calculateSavingsArray[2][3] < 0) {
        document.getElementById("PulseDMOBold").innerHTML = -(Math.ceil(calculateSavingsArray[2][3])) + "%";
        percOffMoreThanText[3].innerHTML = "more";
    } else {
        document.getElementById("PulseDMOBold").innerHTML = Math.ceil(calculateSavingsArray[2][3]) + "%";
        percOffMoreThanText[3].innerHTML = "less";
    }
}

function calculateSpark(usage, controlledLoad, solarFeed, billing){
    //var values = updateUsage(usage, controlledLoad);
    var values = [usage, controlledLoad];
    //console.log("values within calculateSpark is " + values);
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRate = usageGeneral;
    var costGeneral = 30/100 * usageRate;
    var clRate = usageControlled;
    var costCL = 18/100 * clRate;
    var costDailySupply = 110/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;
    //console.log("usageRate is " + usageGeneral);
    //console.log("costGeneral is " + costGeneral);
    //console.log("clRate is " + clRate);
    //console.log("costDailySupply is " + costDailySupply);
    //console.log("solarFitUsage is " + solarFitUsage);
    //console.log("costSolarFit is " + costSolarFit);
    //console.log("billing is " + billing)

    var monthlyPaymentSpa = (costGeneral + costCL + costDailySupply + costSolarFit)/12 * billing;
    return monthlyPaymentSpa;
}

function calculateMillennium (usage, controlled, solarFeed, offPeak, billing){
    //var values = updateUsage(usage, controlled);
    var values = [plan.usage, plan.controlledAmount];
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRatePeak = usageGeneral*(0.7858*(1-offPeak));
    var costUsageRatePeak = 36/100 * usageRatePeak;
    var usageRateEcon = usageGeneral*(0.2142*(1-offPeak));
    var costRateEcon = 18/100 * usageRateEcon;
    var usageHappyHour = usageGeneral * offPeak;
    var costHappyHour = 12/100 * usageHappyHour;
    var clRate = usageControlled;
    var costCL = 12/100 * clRate;
    var costDailySupply = (146.16)/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;

    var monthlyPaymentMil = (costUsageRatePeak + costRateEcon + costHappyHour + costCL + costDailySupply + costSolarFit)/12 * billing;
    return monthlyPaymentMil;
}

function calculateLightning(usage, controlled, solarFeed, offPeak, billing){
    /* var values = updateUsage(usage, controlled); */
    var values = [plan.usage, plan.controlledAmount];
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRatePeakNovMar = usageGeneral * (0.10653*(1-offPeak));
    var costUsageRatePeakNovMar = 125/100 * usageRatePeakNovMar;
    var usageRatePeakAprOct = usageGeneral * (0.15089*(1-offPeak));
    var costUsageRatePeakAprOct = 36/100 * usageRatePeakAprOct;
    var usageRatePrem = usageGeneral * (0.52837*(1-offPeak));
    var costRatePrem = 20/100 * usageRatePrem;
    var usageRateEcon = usageGeneral * (0.21421*(1-offPeak));
    var costRateEcon = 12/100 * usageRateEcon;
    var usageHappyHour = usageGeneral * offPeak;
    var costHappyHour = 8/100 * usageHappyHour;
    var clRate = usageControlled;
    var costCL = 8/100 * clRate;
    var costDailySupply = (146.16)/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit= (-4)/100 * solarFitUsage;

    var monthlyPaymentLig = (costUsageRatePeakNovMar + costUsageRatePeakAprOct + costRatePrem + costRateEcon + costHappyHour + costCL + costDailySupply + costSolarFit)/12 * billing;
    return monthlyPaymentLig;
}

function calculatePulse(usage, controlled, solarFeed, offpeak, billing){
    // var values = updateUsage(usage, controlled);
    var values = [plan.usage, plan.controlledAmount];
    var usageGeneral = values[0];
    var usageControlled = values[1];

    var usageRate = usageGeneral;
    var costUsageRate = 31/100 * usageRate;
    var clRate = usageControlled;
    var costCL = 19/100 * clRate;
    var costDailySupply = 160/100 * 365;
    var solarFitUsage = solarFeed;
    var costSolarFit = (-8)/100 * solarFitUsage;

    var monthlyPaymentPul = (costUsageRate + costCL + costDailySupply + costSolarFit)/12 * billing;
    return monthlyPaymentPul;
}

function calculateComparison(usagePrice, usage, controlledPrice, controlled, supplyPrice, days, solarFit, solarFeed){
    var costUsage = usagePrice/100 * usage;
    var costControlled = controlledPrice/100 * controlled;
    var costSupply = supplyPrice/100 * days;
    var costSolar = solarFit/100 * solarFeed;

    var comparisonMonthly = (costUsage + costControlled + costSupply + costSolar)/12;
    return comparisonMonthly;
}

/* Function which updates the General Usage and Controlled Usage every time the inputs are changed for either of them*/
function updateUsage(usage, controlled) {
    var usageGeneral = 0;
    var usageControlled = 0;
    //console.log("usage is " + usage +" and controlled is " + controlled);
    // If they are using the default values but have a controlled load
    if (controlled == 1) {
        if (usage == 1) {
            usageGeneral = 3100 * 1.05;
            usageControlled = 3100 * 0.45;
        } else if (usage == 2) {
            usageGeneral = 4000 * 1.05;
            usageControlled = 4000 * 0.45;
        } else if (usage == 3) {
            usageGeneral = 4745 * 1.05;
            usageControlled = 4745 * 0.45;
        } else if (usage == 4 || usage == 5) {
            usageGeneral = 6168.5 * 1.05;
            usageControlled = 6168.5 * 0.45;
        } else {
            usageGeneral = usage * 1.05;
            usageControlled = usage * 0.45;
        }
    } 
    // If they have a custom controlled load
    else if (controlled > 1) {
        if (usage == 1) {
            usageGeneral = 3100 * 1.05;
            usageControlled = controlled;
        } else if (usage == 2) {
            usageGeneral = 4000 * 1.05;
            usageControlled = controlled;
        } else if (usage == 3) {
            usageGeneral = 4745 * 1.05;
            usageControlled = controlled;
        } else if (usage == 4 || usage == 5) {
            usageGeneral = 6168.5 * 1.05;
            usageControlled = controlled;
        } else {
            usageGeneral = usage * 1.05;
            usageControlled = controlled;
        }
    }
    // If they dont have a controlled load
    else {
        if (usage == 1) {
            usageGeneral = 3100;      
        } else if (usage == 2) {
            usageGeneral = 4000;
        } else if (usage == 3) {
            usageGeneral = 4745;
        } else if (usage == 4 || usage == 5) {
            usageGeneral = 6168.5;
        } else {
            usageGeneral = usage;
        }
    }
    return [usageGeneral, usageControlled];
}

/* Making detailed rates modal appear depending on which plan link is pressed */
function checkPlan(planId) {
    var modal = document.getElementById("DetailedRatesModal");
    var modalImg = $("#DetailRatesImage");
    var modalImgBus = $("#DetailRatesImageBus");

    if (planId == "SparkDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Spark.jpg");
        $('#DetailedRatesNote').hide();
        $("#DetailedRatesModal").modal('show');

    } else if (planId == "MillenniumDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Millennium.jpg");
        $('#DetailedRatesNote').show();
        $("#DetailedRatesModal").modal('show');

    } else if (planId == "LightningDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Lightning.jpg");
        $('#DetailedRatesNote').show();
        $("#DetailedRatesModal").modal('show');

    } else if (planId == "JupiterDetailRates") {
        modalImg.attr("src", "assets/IOPriceMaps-Jupiter.jpg");
        $('#DetailedRatesNote').show();
        $("#DetailedRatesModal").modal('show');

    } else if (planId == "PulseDetailRates") {
        modalImgBus.attr("src", "assets/IOPriceMaps-Pulse.jpg");
        $("#DetailedRatesModalBus").modal('show');

    } else if (planId == "PulseDetailRates+") {
        modalImgBus.attr("src", "assets/IOPriceMaps-Pulse+.jpg");
        $("#DetailedRatesModalBus").modal('show');

    }
    
}

// Function to show the correct message on loading the page, Specifically for the file upload.
function showUploadResultMessage(message) {
    var errorMsgsMap = new Map();

    errorMsgsMap.set("SuccessfulUpload", new uploadResultMessage("SuccessfulUpload", "Your upload was successful!", "assets/successImg.svg"));
    errorMsgsMap.set("TooBig_2MB", new uploadResultMessage("TooBig_2MB", "Your file was too big! Keep the size under 2MB.", "assets/errorImg.svg"));
    errorMsgsMap.set("Error", new uploadResultMessage("Error", "Something went wrong!<br>Please try again or contact us via email.<br><a class=' underlineLink' href='mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site'>save@ioenergy.com.au</a>", "assets/errorImg.svg"));
    errorMsgsMap.set("TypeError", new uploadResultMessage("TypeError", "Wrong Type. Please only upload .jpg .png or .pdf.", "assets/errorImg.svg"));
    errorMsgsMap.set("Unfinished", new uploadResultMessage("Unfinished", "You didn't give us enough information!", "assets/errorImg.svg"));
    errorMsgsMap.set("Uncaught", new uploadResultMessage("Uncaught", "Ooops, Something went wrong! Please try again!", "assets/errorImg.svg"));

    if (errorMsgsMap.has(message)){
        $('#BillUploadForm').hide();
        $('#UploadStatusMsgContainer').show();

        $('#UploadStatusMsgImg').attr("src", errorMsgsMap.get(message).img);
        $('#UploadStatusMsgText').append(errorMsgsMap.get(message).msg);

        $('#UploadBillModal').modal('show');

    } else {
        $('#BillUploadForm').show();
        $('#UploadStatusMsgContainer').hide();
    }
}
// Class to hold details about each msg
class uploadResultMessage {
    constructor(msgCode, msg, img) {
      this.msgCode = msgCode;
      this.msg = msg;
      this.img = img;
    }
}

// This obj stores all the relevant gtags for each plan
var plansGtags = {'spark' : 'AW-656750783/BSzsCNbVn94BEL_xlLkC',
                 'millennium' : 'AW-656750783/MGuSCI7Vu94BEL_xlLkC', 
                 'lightning': 'AW-656750783/VPNRCMrVu94BEL_xlLkC',
                 'jupiter' : 'AW-656750783/v20pCJmJqt4BEL_xlLkC',
                 'pulse' : 'AW-656750783/hxwxCL7bn94BEL_xlLkC',
                 'pulseplus' : 'AW-656750783/INhtCMn9qd4BEL_xlLkC'
                };

function signup(planName) {
    // All the information needed to create EL Link
    // EL link itself is stored in a global variable elURL
    var postcode = plan.postcode;
    var resOrBus = plan.business;
    var name = '';
    var value = '';
    var offeringcode = '';

    // Check type of plan
    if (resOrBus == false) {
        name = 'Residential';
        value = 'RESIDENT';
    } else {
        name = 'Business';
        value = 'COMPANY';
    }

    // Check the plancode itself
    if (planName == 'spark') {
        offeringcode = 'RESI_IO_SPRK'
    } else if (planName == 'millennium') {
        offeringcode = 'RESI_IO_MIL'
    } else if (planName == 'lightning') {
        offeringcode = 'RESI_IO_LGHT'
    } else if (planName == 'jupiter') {
        offeringcode = 'RESI_IO_JUPT'
    } else if (planName == 'pulse') {
        offeringcode = 'SME_IO_PLS'
    } else if (planName == 'pulseplus') {
        offeringcode = 'SME_IO_PLSP'
    }

    // Check if there is a postcode if not stop the user
    var pc = parseInt(postcode);
    if (!postcode) {
        $("#PostcodeModal").modal('show')

    } else if (pc < 5000 || pc > 5999) { //if we are not in SA
        var pc = parseInt(postcode);
        
        // Get the state the postcode is in
        if (postcode in postcodes){
            switch(postcodes[postcode][1]){
                case "TAS":
                case "WA":
                case "NT":
                    $("#ActiveCampaignModal").hide();
                    $("#PostcodeModal2").modal('show');
                    break;
                case "NSW":
                case "ACT":
                case "VIC":
                case "QLD":
                    $("#PostcodeModal1").modal('show');
                    break;
                case "SA":
                    break;
                default:
                    break;
            }
        }

    } else {

        // Create the elURL for this configuration
        elURL = 'https://energylocals.com.au/sign-up/?Postcode=' + postcode + '&ctName=' + name + '&ctValue=' + value +'&BrandCode=IOENERGY_5064_X&OfferingCode=' + offeringcode;
        console.log(elURL);

        // For time of use plans show modal and only proceed when the modal is acknowledged
        if (planName == 'millennium' || planName == 'lightning' || planName == 'jupiter') {
            //console.log("Disclaimer for Time OF Use Plans");
            $("#TOUDisclaimerModal").modal('show');

        } else { // For non time of use plans call appropriate gtag and redirect to appropriate el page
            
             // Facebook Pixel
            fbq('track', 'Purchase', {
                content_name: planName + '_Plan'
            });

            return sendGtagRedirectToELpage(elURL, plansGtags[planName]);
        }
    }
}
// When the modal is acknowledged redirect
function acknowledgeTOUDisclaimer() {
    var planName = "";

    // Get the plan name from the elURL so we can get the right gtag
    if (elURL.includes('RESI_IO_SPRK')) {
        planName = 'spark';

    } else if (elURL.includes('RESI_IO_MIL')) {
        planName = 'millennium';

    } else if (elURL.includes('RESI_IO_LGHT')) {
        planName = 'jupiter';

    } else if (elURL.includes('RESI_IO_JUPT')) {
        planName = 'pulse';

    } else if (elURL.includes('SME_IO_PLS')) {
        planName = 'pulse';

    } else if (elURL.includes('SME_IO_PLSP')) {
        planName = 'pulseplus';

    }

    // Facebook Pixel
    fbq('track', 'Purchase', {
        content_name: planName + '_Plan'
    });

    // Send Gtag and redirect
    sendGtagRedirectToELpage(elURL, plansGtags[planName]);
}


// Send tracking information to google, gtagcode is unique to each plan
function sendGtagRedirectToELpage(url, gtagCode) {

    // See if there is a postcode
    var postcode = plan.postcode;
    if (!postcode) {

    } else {

        // On successful cookie send prep the EL page to redirect too
        var callback = function () { 
            if (typeof(url) != 'undefined') { window.location = url; } 
        }; 

        // Send tracking cookie to google 
        gtag('event', 'conversion', {
             'send_to': gtagCode, 
             'event_callback': callback 
            }); 

        // Unsure if this is nessasarcy
        return false;
    }
}

// Checks the Type of device accessing the page and from there chooses if a phone number is better or a mobile number is.
function mobileCheck(id, subject) {

    // Identify if mobile device
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    
    
    // Replace spaces with %20 so it is read as a space
    subject = String(subject).replace(/ /gi, "%20");
    console.log(subject);

    // Choose appropriate contact type
    if (check == true) {
        $("#"+id).prop("href", "tel:1300313463;");
    } else {
        $("#"+id).prop("href", "mailto:hello@ioenergy.com.au?subject=" + subject);
    }
    // mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site;
};

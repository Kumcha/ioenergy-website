var app = new Vue({
  el: '#app',
  data: {
    customerTypeCode: "none",
    showResidential: false,
    showBusiness: false,
    mobileNumProvided: false,
    landlineNumProvided: false
  },
  methods: {
    changeCustomerTypeCode (event) {
      if (event.target.value == "resident") {
        this.customerTypeCode = "resident";
        this.showResidential = true;
        this.showBusiness = false;
      } else if (event.target.value == "company") {
        this.customerTypeCode = "business";
        this.showResidential = false;
        this.showBusiness = true;
      } else {
        this.customerTypeCode = "none";
        this.showResidential = false;
        this.showBusiness = false;
      }
    },
    mobileNumFieldChanged (event) {
      // Put more validators for mobile number here
      if (event.target.value != "") {
        this.mobileNumProvided = true;
      } else {
        this.mobileNumProvided = false;
      }
    },
    landlineNumFieldChanged (event) {
      // Put more validators for landline number here
      if (event.target.value != "") {
        this.landlineNumProvided = true;
      } else {
        this.landlineNumProvided = false;
      }
    }
  }
});

// If testing, set this to true. In non-test development environment, set 
// this to the actual location of the HTTP request.
const testEnv = true;

function submitForm() {
  xmlhttp = new XMLHttpRequest();

  const form = document.getElementById("SignUpForm");
  const formData = new FormData(form);

  var params = "";

  // NEEDS TO SEND promo_allowed IF BOX IS NOT CHECKED
  var i = 0;
  for (var pair of formData.entries()) {
    if (pair[0] != "identification" && pair[0] != "address_auto_input") {
      
      if (i > 0) {
        params += "&";
      }
      params += pair[0] + "=" + pair[1];
      console.log(pair[0]);
    }
    i += 1;
  }
  var promoCheckedStatus = document.getElementById("promo_allowed").checked;
  if (promoCheckedStatus) {
    params += "&promo_allowed=E";
  } else {
    params += "&promo_allowed=N";
  }

  if (testEnv) {
    xmlhttp.open("POST", "http://localhost:8080/", true);
  } else {
    xmlhttp.open("POST", "", true);
  }
  xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      alert(xmlhttp.responseText);
    }
  }
  xmlhttp.send(params);
}
Vue.component('welcome-content', {
    template: `
        <div id="WelcomeContentDiv">
            <h1>Cleaner, Cheaper, Smarter</h1>
            <p>Pay far less money to use vastly cleaner electricity, with the peace of mind that we will help 
            you improve your energy outcomes.</p>
            <button>Find out more</button>
        </div>
    `
});

Vue.component('welcome-section', {
    template: `
        <div id="WelcomeDiv">
            <img src="assets/landingPageWelcome.png" />
            <div id="WelcomeFadeDiv"></div>
            <welcome-content></welcome-content>
        </div>
    `
});

Vue.component('content-section', {
    props: ['content'], // title, paragraphs
    template: `
        <div class="contentDiv">
            <div class="contentTitleDiv">
                <div></div>
                <h1>{{ content.title }}</h1>
            </div>
            <p v-for="para in content.paras" v-html="para"></p>
        </div>
    `
});

Vue.component('how-it-works-step', {
    props: ['content', 'num'],
    template: `
        <div class="howItWorksInner col-md-4 col-12">
            <!--number and image div-->
            <div>
                <!--div below will be an image later on-->
                <div class="howItWorksImageDiv">
                    <p v-bind:style="{ right: content.shiftRight }">{{ num }}</p>
                    <img v-bind:src="content.image" />
                </div>
            </div>
            <p class="howItWorksExplainerText"><strong>{{ content.title }}</strong></p>
            <!--https://forum.vuejs.org/t/how-do-i-make-an-html-tag-inside-a-data-string-render-as-an-html-tag/13074/4-->
            <p class="howItWorksExplainerText" v-html="content.detail"></p>
        </div>
    `
});

Vue.component('plan-card', {
    props: ['contents'],
    template: `
        <div class="planCard col">
            <h1>Save &#36;{{ contents.saveAmount }} pa</h1>
            <p class="percOffPara">{{ contents.percLess }}&percnt; less than the Reference Price.</p>
            <h1>{{ contents.name }}</h1>
            <p class="planDescriptionPara">{{ contents.description }}</p>
            <ul>
                <li v-for="i in contents.features.length" v-html="contents.features[i - 1]"></li>
            </ul>
            <button>Find out more</button>
        </div>
    `
});

var app = new Vue({
    el: '#app',
    data: {
        whyIO: {
            title: 'Why IO Energy?',
            paras: [
                `
                    We offer you <strong>clean energy cheap</strong>! By unlocking the power of 
                    a smart meter you can get access to some of the cheapest, cleanest energy 
                    on the grid.
                `,
                `
                    Our customer service team will help <strong>make the complex seem 
                    simple</strong>. We offer personalised savings calculations on your 
                    energy bill.
                `,
                `
                    We offer transparency. Our pricing model and billing reflect this. 
                    <strong>We&#39;re sick of hidden charges and fees</strong>.
                `,
                `
                    <a class="underlineLink" href="/HowItWorks/">Tell me more</a>
                `
            ]
        },
        whySmartMeters: {
            title: 'Why Smart Meters matter',
            paras: ['Text!', 'More text!', 'Heck yes smart meters XD']
        },
        customers: {
            title: 'What our customers have to say',
            paras: []
        },
        howItWorks: [
            {
                title: 'Calculate your energy usage',
                image: 'assets/howItWorksCalculator.png',
                detail: `
                    Use our handy <strong><u>calculator</u></strong> to work out your average 
                    energy consumption.
                `,
                shiftRight: '40px'
            },
            {
                title: 'Pick a plan',
                image: 'assets/howItWorksPlan.png',
                detail: `
                    Choose the <strong><u>plan</u></strong> that best suits your energy needs.
                `,
                shiftRight: '55px'
            },
            {
                title: 'Contact us',
                image: 'assets/howItWorksPhone.png',
                detail: `
                    <strong><u>Talk with</u></strong> one of our customer care team to sign 
                    up to IO Energy.
                `,
                shiftRight: '55px'
            }
        ],
        planCardContents: [
            {
                saveAmount: '194',
                percLess: '11',
                name: 'Millennium',
                description: 'ideal for off peak use',
                features: [
                    'Smart Meter',
                    'Time-of-use charges',
                    '<strong>As low as 12 cents per kWh</strong><sup>i</sup>',
                    'Uses 90&percnt; clean energy'
                ]
            },
            {
                saveAmount: '238',
                percLess: '13',
                name: 'Lightning',
                description: 'ideal for off peak use',
                features: [
                    'Smart Meter',
                    'Time-of-use charges',
                    '<strong>As low as 18 cents per kWh</strong><sup>i</sup>',
                    'Uses 90&percnt; clean energy'
                ]
            },
            {
                saveAmount: '231',
                percLess: '13',
                name: 'Spark',
                description: 'flat rate',
                features: [
                    'Smart Meter',
                    'Time-of-use charges',
                    '<strong>34 cents per kWh</strong>',
                    'Uses 76&percnt; clean energy'
                ]
            }
        ],
        customerExperiences: [
            {
                name: 'Shauna',
                image: 'assets/shauna.png',
                experience: `aigd awg djagdua  dajdv awjvhw vh vhjv hjv h vhgvhg hg h vhg vhvhvhg vhvhg vhvh`
            },
            {
                name: 'Robin',
                image: 'assets/robin.png',
                experience: `i agi uwai awi wgu i uy guy guy guy gu guyguyfajfdgaiugdalwhdlaw do i aiw dgak`
            },
            {
                name: 'Jordan',
                image: 'assets/jordan.png',
                experience: `a iwgd iagd iag wi wiu iwua iuwdgiauwgd iaiwiwug iuadg iuwa giwgiuwg di giu g`
            }
        ],
        payLess: {
            title: 'Pay less for cleaner electricity',
            paras: [
                `
                    <strong>The more daytime electricity you use, the better off we&#39;ll all be.</strong> 
                    Over the last year South Australia&#39;s electricity was 60&percnt; renewable. But 
                    importantly, that changes over the day. At 1pm the grid was a massive 80&percnt; 
                    renewable, and at 7pm it was a mere 38&percnt; renewable.
                `,
                `
                    <strong>We reward you for using that ultra-clean energy.</strong> Time-of-use pricing 
                    allows us to reward you for using cleaner electricity. Many of our household customers 
                    are literally saving thousands of dollars a year by taking advantage of this system, and 
                    running appliances and devices during the daytime, when solar generation is at its highest.
                `
            ]
        }
    }
});

//Parallax for Welcome section Rellax JS
(function(n,h){"function"===typeof define&&define.amd?define([],h):"object"===typeof module&&module.exports?module.exports=h():n.Rellax=h()})("undefined"!==typeof window?window:global,function(){var n=function(h,p){var a=Object.create(n.prototype),l=0,r=0,k=0,t=0,c=[],u=!0,B=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||window.oRequestAnimationFrame||function(a){return setTimeout(a,1E3/60)},q=null,C=window.cancelAnimationFrame||
window.mozCancelAnimationFrame||clearTimeout,D=window.transformProp||function(){var a=document.createElement("div");if(null===a.style.transform){var b=["Webkit","Moz","ms"],e;for(e in b)if(void 0!==a.style[b[e]+"Transform"])return b[e]+"Transform"}return"transform"}();a.options={speed:-2,center:!1,wrapper:null,relativeToWrapper:!1,round:!0,vertical:!0,horizontal:!1,callback:function(){}};p&&Object.keys(p).forEach(function(d){a.options[d]=p[d]});h||(h=".rellax");var m="string"===typeof h?document.querySelectorAll(h):
[h];if(0<m.length){a.elems=m;if(a.options.wrapper&&!a.options.wrapper.nodeType)if(m=document.querySelector(a.options.wrapper))a.options.wrapper=m;else{console.warn("Rellax: The wrapper you're trying to use doesn't exist.");return}var w=function(){for(var d=0;d<c.length;d++)a.elems[d].style.cssText=c[d].style;c=[];r=window.innerHeight;t=window.innerWidth;x();for(d=0;d<a.elems.length;d++){var b=a.elems[d],e=b.getAttribute("data-rellax-percentage"),g=b.getAttribute("data-rellax-speed"),h=b.getAttribute("data-rellax-zindex")||
0,l=b.getAttribute("data-rellax-min"),n=b.getAttribute("data-rellax-max"),v=a.options.wrapper?a.options.wrapper.scrollTop:window.pageYOffset||document.documentElement.scrollTop||document.body.scrollTop;a.options.relativeToWrapper&&(v=(window.pageYOffset||document.documentElement.scrollTop||document.body.scrollTop)-a.options.wrapper.offsetTop);var f=a.options.vertical?e||a.options.center?v:0:0,k=a.options.horizontal?e||a.options.center?a.options.wrapper?a.options.wrapper.scrollLeft:window.pageXOffset||
document.documentElement.scrollLeft||document.body.scrollLeft:0:0;v=f+b.getBoundingClientRect().top;var m=b.clientHeight||b.offsetHeight||b.scrollHeight,p=k+b.getBoundingClientRect().left,q=b.clientWidth||b.offsetWidth||b.scrollWidth;f=e?e:(f-v+r)/(m+r);e=e?e:(k-p+t)/(q+t);a.options.center&&(f=e=.5);g=g?g:a.options.speed;e=y(e,f,g);b=b.style.cssText;f="";0<=b.indexOf("transform")&&(f=b.indexOf("transform"),f=b.slice(f),f=(k=f.indexOf(";"))?" "+f.slice(11,k).replace(/\s/g,""):" "+f.slice(11).replace(/\s/g,
""));c.push({baseX:e.x,baseY:e.y,top:v,left:p,height:m,width:q,speed:g,style:b,transform:f,zindex:h,min:l,max:n})}u&&(window.addEventListener("resize",w),u=!1);z()},x=function(){var d=l,b=k;l=a.options.wrapper?a.options.wrapper.scrollTop:(document.documentElement||document.body.parentNode||document.body).scrollTop||window.pageYOffset;k=a.options.wrapper?a.options.wrapper.scrollLeft:(document.documentElement||document.body.parentNode||document.body).scrollLeft||window.pageXOffset;a.options.relativeToWrapper&&
(l=((document.documentElement||document.body.parentNode||document.body).scrollTop||window.pageYOffset)-a.options.wrapper.offsetTop);return d!=l&&a.options.vertical||b!=k&&a.options.horizontal?!0:!1},y=function(d,b,e){var c={};d=100*e*(1-d);b=100*e*(1-b);c.x=a.options.round?Math.round(d):Math.round(100*d)/100;c.y=a.options.round?Math.round(b):Math.round(100*b)/100;return c},A=function(){x()&&!1===u&&z();q=B(A)},z=function(){for(var d,b=0;b<a.elems.length;b++){d=y((k-c[b].left+t)/(c[b].width+t),(l-
c[b].top+r)/(c[b].height+r),c[b].speed);var e=d.y-c[b].baseY,g=d.x-c[b].baseX;null!==c[b].min&&(a.options.vertical&&!a.options.horizontal&&(e=e<=c[b].min?c[b].min:e),a.options.horizontal&&!a.options.vertical&&(g=g<=c[b].min?c[b].min:g));null!==c[b].max&&(a.options.vertical&&!a.options.horizontal&&(e=e>=c[b].max?c[b].max:e),a.options.horizontal&&!a.options.vertical&&(g=g>=c[b].max?c[b].max:g));a.elems[b].style[D]="translate3d("+(a.options.horizontal?g:"0")+"px,"+(a.options.vertical?e:"0")+"px,"+c[b].zindex+
"px) "+c[b].transform}a.options.callback(d)};a.destroy=function(){for(var d=0;d<a.elems.length;d++)a.elems[d].style.cssText=c[d].style;u||(window.removeEventListener("resize",w),u=!0);C(q);q=null};w();A();a.refresh=w;return a}console.warn("Rellax: The elements you're trying to select don't exist.")};return n});

var rellax = new Rellax('.rellax');
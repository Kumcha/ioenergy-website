window.onload = function() {
    // mobileCheck()
    displayFooter()
    displayNav()
    AOS.init();
//    if (screen.width >= 768) {
//        changeColClasses(true);
//    }
}

//window.onresize = function() {
//    if (innerWidth >= 768) {
//        changeColClasses(true);
//    } else if (innerWidth < 768) {
//        changeColClasses(false);
//    }
//}

//function changeColClasses(makeBig) {
//    // Used to change the class of an element:
//    // https://stackoverflow.com/questions/195951/how-can-i-change-an-elements-class-with-javascript
//    var elem = document.getElementById('EmailCol');
//    if (makeBig == true) {
//        elem.classList.remove('col-sm-6');
//        elem.classList.add('col-sm-7');
//    } else {
//        elem.classList.remove('col-sm-7');
//        elem.classList.add('col-sm-6');
//    }
//}


function displayNav(){
    // let check = mobileCheck()
    // let contactHref = ""
    
    // if (check == true) {
    //     contactHref = "tel:1300313IOE;"
    // } else {
    //     contactHref = "mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site"
    // }

    navHTML = `
    <style>
    .navbar {
        background-color: white;
        position: fixed;
        left: 0;
        right: 0;
        opacity: .97;
        z-index: 100;
    }
    
    #NavLogo {
        height: 25pt;
        margin: 10pt ;
        padding: 0pt;
    }
    
    #NavLogo:hover {
        text-decoration: none;
    }
    
    .navItem {
        text-align: center;
        margin-right: 20pt;
        margin-left: 20pt;
        font-family: 'Quicksand', sans-serif;
        font-weight: 500;
        color: black;
        padding: 10pt;
    }
    .navItem:hover {
        color: #FD5C8F;
    }

    @media (min-width: 992px) {
        .NavUnderlineLink {
            position: relative;
            text-decoration: none;
            display: inline-block;
        }
        
        .NavUnderlineLink:after {
            display: block;
            content: '';
            border-bottom: solid 1px;
            -webkit-transform: scaleX(0);
            transform: scaleX(0);
            -webkit-transition: -webkit-transform 250ms ease-in-out;
            transition: -webkit-transform 250ms ease-in-out;
            transition: transform 250ms ease-in-out;
            transition: transform 250ms ease-in-out, -webkit-transform 250ms ease-in-out;
            -webkit-transform-origin: 100% 50%;
            transform-origin: 100% 50%;
        }
        
        .NavUnderlineLink:hover:after {
            -webkit-transform: scaleX(1);
            transform: scaleX(1);
        
            -webkit-transform-origin: 0 50%;
            transform-origin: 0 50%;
        }
    }

    </style>
    <div class="container">
    <!--io Logo-->
    <div class="navbar-brand" href="#">
        <a href="/"><img src="../CommonResources/ioEnergyLogo.png" id="NavLogo" alt="IO Energy Logo"></a>
    </div>

    <!--Hamburger Menu-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation" style="border: 0pt;">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <!--Links-->
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="navItem navUnderlineLink" href="/">Home</a>
            <a class="navItem navUnderlineLink" href="/OurPlans/">Pricing</a>
            <a class="navItem navUnderlineLink" href="/HowItWorks/">How It Works</a>
            <a class="navItem navUnderlineLink" href="/AboutUs/">About Us</a>
            <a class="navItem navUnderlineLink" href="/ContactUs/">Contact</a>

        </div>
    </div>
    
    </div>
    `
    document.querySelector(".navbar").innerHTML = navHTML;
}

function displayFooter(){
    footerHTML = `
        <div class="container">

            <style>
            #Footer {
                background-color: whitesmoke;
                padding: 40pt;
                font-size: 10pt;
                font-family: 'Quicksand', sans-serif;
            }
            #FooterLogo {
                width: 30pt; 
                padding: 0pt;
            }
            .footerColumn {
                padding: 10pt; padding-right: 30pt;
            }
            .footerLink {
                color: black;
                font-family: 'Quicksand', sans-serif;
            }
            .footerLink:hover {
                color: black;
            }
            #Tribute {
                text-align: right;
                font-size: 8pt;
                display: block;
                position: absolute;
                bottom: 0;
                right: 0;
                margin-bottom: 0;
                padding: 5pt;
            }
            .underlineLink {
                position: relative;
                text-decoration: none;
                display: inline-block;
            }
            
            a:hover {
                text-decoration: none;
            }
            
            .underlineLink:after {
                display: block;
                content: '';
                border-bottom: solid 1px;
                -webkit-transform: scaleX(0);
                transform: scaleX(0);
                -webkit-transition: -webkit-transform 250ms ease-in-out;
                transition: -webkit-transform 250ms ease-in-out;
                transition: transform 250ms ease-in-out;
                transition: transform 250ms ease-in-out, -webkit-transform 250ms ease-in-out;
                -webkit-transform-origin: 100% 50%;
                transform-origin: 100% 50%;
            }
            
            .underlineLink:hover:after {
                -webkit-transform: scaleX(1);
                transform: scaleX(1);
            
                -webkit-transform-origin: 0 50%;
                transform-origin: 0 50%;
            }
        
            .waitlistEmail {
                font-size: 10pt;
                background-color: #2B265A;
                border-color: #1c183f;
                opacity: 0.8;
                color: white;
        
            }
        
            .waitlistEmail:focus {
                font-size: 10pt;
                border-color: #1c183f;
                box-shadow: inset 0 1px 1px #453c92, 0 0 8px #4a4296;
                background-color: #2B265A;
                // opacity: 0.7;
                color: white;
            }
        
            .waitlistEmail::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: white;
                opacity: 1; /* Firefox */
              }
              
            .waitlistEmail:-ms-input-placeholder { /* Internet Explorer 10-11 */
                color: white;
            }
        
            .btnPurpleOutline {
                border-color: #2B265A;
                font-size: 10pt;
                color: #2B265A;
                opacity: 0.8;
            }
        
            .btnPurpleOutline:hover  {
                font-size: 10pt;
                border-color: #1c183f;
                box-shadow: inset 0 1px 1px #453c92, 0 0 8px #4a4296;
                background-color: #2B265A;
                // opacity: 0.7;
                color: white;
            }
        
            .btnPurpleOutline:focus  {
                border-color: #1c183f;
                box-shadow: inset 0 0px 0px #453c92, 0 0 0px #4a4296;
            }

            #BusinessStatement {
                text-align: center;
            }
        
            </style>

            <div class="container">
            <div class="row">
                <div class="col">
                    <p id=BusinessStatement>
                        &#169; IO Energy Pty Ltd ABN 94 639 438 510 <br>
                        The retailer and provider of energy to customers of IO Energy products is Energy Locals Pty Ltd <br>
                        ACN 606 408 879, a licensed electricity retailer. <br>
                        <br>
                        <br>
                    </p>
                </div>
            </div>
        
            <div class="row">
                <div class="col-sm footerColumn">
                    <p><b>IO Energy</b></p>
                    <p style="text-align: left;">IO Energy&#39;s mission is to help people maximise the use of cheap and clean energy at home, to accelerate our transition to a zero-carbon future.</p>
                </div>
        
                <div class="col-sm footerColumn">
                    <p><a class="footerLink underlineLink" href="/"><b>Home</b></a></p>
                    <p>
                        <a class="footerLink underlineLink" href="/OurPlans/">Pricing</a><br>
                        <a class="footerLink underlineLink" href="/HowItWorks">How It Works</a><br>
                        <a class="footerLink underlineLink" href="/AboutUs/">About Us</a><br>
                        <a class="footerLink underlineLink" href="/ContactUs/">Contact</a>
                    </p>
                </div>
                <div class="col-sm footerColumn">
                <p><b>More Information</b></p>
                <p>
                    <a class="footerLink underlineLink" 
                        href="https://energylocals.com.au/terms/?_ga=2.200006702.1439639648.1596627248-2027481711.1596515158" target="_blank">Terms and conditions</a><br>
                    <a class="footerLink underlineLink" 
                        href="../CommonResources/200904 IO Energy Privacy Policy.pdf" target="_blank">Privacy statement</a><br>
                    <a class="footerLink underlineLink" 
                        href="../BPID/">Basic Plan Info Documents</a><br>
                    <a class="footerLink underlineLink" 
                        href="https://energylocals.com.au/covid-19-energy-support-plans/" target="_blank">COVID-19</a><br>
                    <a class="footerLink underlineLink" 
                        href="https://energylocals.com.au/energy-concessions/" target="_blank">Concessions</a><br>
                    <a class="footerLink underlineLink" 
                        href="https://energylocals.com.au/life-support/" target="_blank">Life Support</a><br>
                    <a class="footerLink underlineLink" 
                        href="https://energylocals.com.au/hardship/" target="_blank">Hardship</a><br>

                </p>


            </div>
                <div class="col-sm footerColumn">
                    <p><b>Connect</b></p>
                    <p>
                        <a class="footerLink underlineLink" 
                            href="https://twitter.com/ioenergy_aus" target="_blank">Twitter</a><br>
                        <a class="footerLink underlineLink" 
                            href="https://www.facebook.com/ioenergysolutions/" target="_blank">Facebook</a><br>
                        <a class="footerLink underlineLink" 
                            href="https://www.linkedin.com/company/37562504/" target="_blank">LinkedIn</a><br>
                        <a class="footerLink underlineLink" 
                            href="https://www.instagram.com/ioenergy/" target="_blank">Instagram</a><br>
                        <a class="footerLink underlineLink" href="mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site">Email Us</a><br>
                        <a class="footerLink underlineLink" href="tel:1300313IOE;">Call Us</a><br>
                    </p>
                </div>
            </div>
        
            <div class="row">
                <div class="col">
                    <img id="FooterLogo" src="../CommonResources/Purple%20Logo%20Small.png" alt="ioEnergy Logo" style="width: 30pt; padding: 0pt;"/>
                </div>
                <div class="col">
                    <p id="Tribute">Made w/ &#10084;&#65039; by <b>Hackamatix</b></p>
                </div>
            </div>
        
            </div>
            <script type="text/javascript">
            (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
            vgo('setAccount', '26207220');
            vgo('setTrackByDefault', true);
    
            vgo('process');
            </script>
    `
    document.querySelector("#Footer").innerHTML = footerHTML;    
}

function mobileCheck() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    
    return check
    // mailto:hello@ioenergy.com.au?subject=Enquire%20From%20Site;
};